colors_64 <- c("#E64B35FF", "#4DBBD5FF", "#00A087FF", "#3C5488FF", "#F39B7FFF", 
                "#8491B4FF", "#91D1C2FF", "#DC0000FF", "#7E6148FF", "#B09C85FF",
                "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
                "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
                "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
                "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
                "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
                "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
                "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
                "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",
                "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
                "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
                "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
                "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
                "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C")

###################################################################
#              Function Description Template                      #
###################################################################

#--------------
# FUNCTION
#    the function name
# COMMENT
#    useful information that people need to know about the function
# REQUIRES
#    any and all required packages
# VARIABLES
#    description of all args/variables that the function accepts
# RETURNS
#    description of what the function returns

#--------------
# FUNCTION
#    
# COMMENT
#    
# REQUIRES
#    
# VARIABLES
#    
# RETURNS
#    


###################################################################
#                      Helper Functions                           #
###################################################################

#--------------
# FUNCTION
#    strToRegex -- a function for regexification of a complex string
# COMMENT
#    this is messy and experimental but functional
# REQUIRES
#    magrittr
# VARIABLES
#    x = a string which needs to be regexified
# RETURNS
#    a regular expression 
strToRegex <- function(x){
    require(magrittr, quietly = T)
    origStr <- x
    xChars <- strsplit(x, "") %>% unlist
    subCharIdx <- grep("[[:punct:]]", xChars)
    wsCharIdx <- grep("\\s", xChars)
    subChars <- xChars[subCharIdx]
    regexChars <- paste0("\\", subChars)
    xChars[subCharIdx] <- regexChars
    xChars[wsCharIdx] <- "\\s"
    regexStr <- paste(xChars, collapse = "")
    return(regexStr)
}


#--------------
# FUNCTION
#    is.empty
# COMMENT
#    tests if a list is just a bunch of empty elements. e.g. [[1]] NULL [[2]] NULL etc
# REQUIRES
#    
# VARIABLES
#    x is a list to check the length of filled elements
# RETURNS
#    Logical: TRUE/FALSE
is.empty <- function(x){ 
    stopifnot(is.list(x))
    ifelse(length(Filter(length, x)) < 1, TRUE, FALSE)
}   

#--------------
# FUNCTION
#    melt
# COMMENT
#    masks reshape2 melt with melt that doesn't spit out a message
#    #TODO: replace this with something not reshape2
# REQUIRES
#    reshape2
# VARIABLES
#    x is a data.frame, array, or list
# RETURNS
#    x in long format 
melt <- function(x){ suppressMessages(reshape2::melt(x)) }

#--------------
# FUNCTION
#    hline and vline
# COMMENT
#    these functions support the volcano plotting fucntion
#    used for drawing horizontal or vertical lines on a plotly plot
# REQUIRES
#    
# VARIABLES
#    
# RETURNS
#    
# helper function to draw vertical line
vline <- function(x = 0, color = "red") {
    list(type = "line", 
         x0 = x, x1 = x,
         y0 = 0, y1 = 1, 
         yref = "paper", 
         line = list(color = color, dash = "dot")) 
}
# helper function to draw horizontal line
hline <- function(y = 0, color = "blue") {
    list(type = "line", 
         x0 = 0, x1 = 1, 
         y0 = y, y1 = y,
         xref = "paper", 
         line = list(color = color, dash = "dash")) 
}

###################################################################
#                        Main Functions                           #
###################################################################

#--------------
# FUNCTION
#    generateCorrPlot
# COMMENT
#    
# REQUIRES
#    
# VARIABLES
#    
# RETURNS
#    
generateCorrPlot <- function(corrData, corrLabs, corrMethod, corrColors, corrDend, corrCut, corrMain){
    if(corrCut == 0){ corrCut <- NA }
    corrPlotDat <- 
        cor(corrData, method = corrMethod, use = "pairwise.complete.obs") %>% 
        heatmapr(labCol = corrLabs,
                 labRow = corrLabs, 
                 k_row = corrCut, 
                 k_col = corrCut,
                 dendrogram = corrDend) 
    corrPlot <- 
        heatmaply(corrPlotDat, 
                  colors = viridis(n = 256, option = corrColors),
                  plot_method = "plotly", 
                  key.title = "Correlation\nCoefficient",
                  main = corrMain) %>% 
        colorbar(tickfont = list(size = 12), thickness = 20, len = 0.4, which = 1)
    return(list(corrPlotDat, corrPlot))
}

#--------------
# FUNCTION
#    generateCorrPlot
# COMMENT
#    
# REQUIRES
#    
# VARIABLES
#    
# RETURNS
#    
generateCustomHeat <- function(heatDat, heatCols, kTitle, heatTitle, covCols, dTick, 
                               k_row_cut, heatCovariatesVar, colLen, whichDendrogram, ...){
    if(k_row_cut == 0){ k_row_cut <- NA }
    if(whichDendrogram == "column" || whichDendrogram == "none"){
        k_row_cut <- NA
    }
    # put in set seed here
    set.seed(2)
    custHeatPlotDat <- heatmapr2(x = heatDat, 
                                 col_side_colors = heatCovariatesVar,
                                 k_row = k_row_cut,
                                 dendrogram = whichDendrogram,
                                 ...)
    set.seed(2)
    custHeatPlot <- heatmaply(x = custHeatPlotDat, 
                              colors = heatCols,
                              plot_method = "plotly",
                              key.title = kTitle,
                              main = heatTitle,
                              col_side_palette = covCols) %>% 
        layout(margin = list(l = 100, b = 100))
    if(is.null(heatCovariatesVar)){
        custHeatPlot <- colorbar(custHeatPlot, tickfont = list(size = 12), len = 0.3,
                                 tickmode = "linear", tick0 = floor(min(heatDat)),
                                 dtick = dTick, which = 1)
    } else {
        custHeatPlot <- 
            colorbar(custHeatPlot, tickfont = list(size = 12), len = colLen, which = 1) %>%
            colorbar(custHeatPlot, tickfont = list(size = 12), len = 0.3,
                     tickmode = "linear", tick0 = floor(min(heatDat)),
                     dtick = dTick, which = 2)
    }
    return(list(custHeatPlotDat, custHeatPlot))
}

#--------------
# FUNCTION
#    sortTransformedCounts
# COMMENT
#    replaces varianceSortedTransformedCounts since it can now be sorted by 
#    bcv or pairwise logfc
# REQUIRES
#    edgeR
# VARIABLES
#    edgeROut -- the object output by the getDE() function
#    sortMethod -- method for ordering data either "variance" or "logFC"
# RETURNS
#    transformed counts re-ordered by logFC or variance
sortTransformedCounts <- function(edgeROut = myEdgeROut, 
                                  sortMethod = c("variance", "logFC")){
    if(sortMethod == "logFC"){
        # this isn't actual logFC, just an estimate
        normDat <- edgeR::cpmByGroup(edgeROut$dgeList) 
        # compute pairwise "logFC" between groups
        hold <- list(); k <- 1
        for (i in 2:ncol(normDat)) {
                # for each pair of columns (groupwise CPMs) calculate the log ratio
                hold[[k]] <- apply(normDat[, c(i - 1, i)], 1, function(x){log((x[2]/x[1])+0.2)})
                k <- k+1
        }
        x <- 
            do.call(cbind, hold) %>% 
            # order by average logFC across all groups
            apply(X = ., MARGIN = 1, FUN = function(x){mean(x^2)}) %>% 
            order(., decreasing = T) %>% 
            edgeROut$transformedCounts[., ]
    }
    else if(sortMethod == "variance"){
        x <-
            sqrt(edgeROut$dgeList$tagwise.dispersion) %>% 
            order(., decreasing = T) %>% 
            edgeROut$transformedCounts[., ]
    }
    return(as.data.frame(x))
}


#--------------
# FUNCTION
#    geneTableRender
# COMMENT
#    generates a fancy table for gene expression data
# REQUIRES
#    DT, dplyr
# VARIABLES
#    table -- a data.frame of gene expression data
#    title -- a string title
# RETURNS
#    hopefully a datatable...
geneTableRender <- function(table, title = ""){
    if(!is.data.frame(table)){
        stop(paste0("Data frame is expected but ", class(table)," found instead"))
    }
    if("gene_id" %in% colnames(table)){
        table$gene_id <- paste0('<a href="http://www.ensembl.org/id/', table$gene_id, 
                                '" target="_blank">', table$gene_id, '</a>')
    } else { 
        table$gene_id <- paste0('<a href="http://www.ensembl.org/id/', rownames(table), 
                                '" target="_blank">', rownames(table), '</a>') 
        table <- dplyr::select(table, gene_id, everything())
    }
    dt <- renderDataTable({
        DT::datatable(table,
                      class = "compact stripe",
                      escape = FALSE,
                      caption = title,
                      filter = 'top',
                      rownames = F,
                      extensions = c('ColReorder','FixedColumns'), 
                      options = list(scrollX = T,
                                     pageLength = 5,
                                     colReorder = T,
                                     fixedColumns = list(leftColumns = 1))) %>%
        DT::formatSignif(columns = which(sapply(table, is.double)), digits = 4)
    }) 
    return(dt)
}    

#--------------
# FUNCTION
#    gtGene2
# COMMENT
#    
# REQUIRES
#    dplyr, reshape2, ggplot2, scales
# VARIABLES
#    edgeRObject - the output from the getDE() function
#    glist - a vector of gene names
#    gtable - a dataframe with gene expression in columns. 
#             In this context it takes data from out_list$rawCount or out_list$transformedCount
#    ytitle - Y-axis title - is not currently in use
#    ptitle - plot title
#    byGroup - 
#    colorPal - a color palette
#    pltcols - number of bargraph in a row
# RETURNS
#    a ggplot
gtGene2 <- function(edgeRObject = myDEOutput,
                    glist = NULL, 
                    gExpression = "CPM",
                    # gtable = NULL, 
                    byGroup = "All", 
                    scaleYes = T,
                    ptitle = "", 
                    # ytitle = "", 
                    colorPal = npgColors,
                    plotcols = 5) {
    require(dplyr, quietly = T); require(reshape2, quietly = T); 
    require(ggplot2, quietly = T); require(scales, quietly = T)

    if(scaleYes){ scale = "free" } else { scale = "free_x" }
    
    ### generate gtable within the function
    switch(gExpression,
        rawCounts = {
            gtable <- 
                glob2rx(glist) %>% 
                paste(., collapse = "|") %>% 
                grep(., toupper(edgeRObject$rawCounts$gene_name)) %>% 
                edgeRObject$rawCounts[.,]
            ytitle <- "Raw Counts"
        },
        CPM = {
            gtable <- 
                glob2rx(glist) %>% 
                paste(., collapse = "|") %>% 
                grep(., toupper(edgeRObject$transformedCounts$gene_name)) %>% 
                edgeRObject$transformedCounts[.,]
            ytitle <- "Log2 CPMs"
        }
    )
    
    gtable$gene_name <-  toupper(gtable$gene_name)
    lookup <- data.frame(sample = rownames(edgeRObject$dgeList$samples), 
                         group = edgeRObject$dgeList$samples$group)
    
    mat_melt <- 
        melt(gtable) %>% 
        mutate(., Group = factor(lookup$group[match(variable, lookup$sample)], 
                                 levels = levels(edgeRObject$dgeList$samples$group)),
                  gene_name = as.factor(gene_name)) %>% rename(.,sample_id = variable)
    
   if("All" %in% byGroup){byGroup =  levels(mat_melt$Group) 
       } else {  mat_melt <- droplevels(mat_melt[mat_melt$Group %in% byGroup,])} 
    
   mat_melt_s <- 
        group_by(mat_melt, gene_name, Group) %>% 
        summarize(., sd = sd(value, na.rm = T), value = mean(value))
   
   #length of the longest X-label
   labLength <- round(max(sapply(unique(as.character(mat_melt$Group)),nchar))*0.45) 
   rowScaling <-  ceiling(dim(gtable)[1]/plotcols)    # number of rows given plotcols column
   # number of groups taken directly from a table that will be used in ggplot
   #gGroups <- length(unique(mat_melt_s$Group))        
   geneNumber <- length(unique(mat_melt_s$gene_name)) # number of genes to be  ploted
   
   #ignore warning about "unknown aestics" - it is just undocumented
   geneBar <- 
        ggplot(data = mat_melt, 
               aes(x = factor(Group,levels = byGroup), y = value)) + theme_classic() +
        geom_point(
               aes(text = paste("Sample ID:", mat_melt$sample_id)), size = 2, alpha = 0.7) +
        geom_bar(data = mat_melt_s, 
               aes(fill = factor(Group,levels = byGroup)), color = "black", stat = "identity",
                 width = 0.8, alpha = 0.8, position = position_dodge(width = 0.3)) +
        geom_errorbar(data = mat_melt_s, 
                      aes(ymin = value, ymax = value+sd), width = 0.1, 
                      position = position_dodge(width = 0.3)) +
        facet_wrap(. ~gene_name, scales = "free", ncol = plotcols, drop = T) +
        labs(title = ptitle, x = "", y = ytitle) + 
        scale_fill_manual(values = colorPal) +
        scale_y_continuous(breaks = scales::pretty_breaks(n = 6)) + 
        #scale_x_discrete(labels = as.character(mat_melt_s$Group)) +
        theme(panel.spacing.y = unit(labLength, "char"),
              strip.text = element_text(size = 10, face = "bold"),
              legend.position = "none", 
              plot.margin = margin(l = 35),
              axis.text.x = element_text(angle = 45, hjust = 1, vjust = 0.5, size = 10))  
   
   geneBar <- ggplotly(geneBar, height = rowScaling*350, tooltip = c("text","y")) %>% 
               config(displayModeBar = F)  

   #The section below from here to return(geneBar) involves manual editing or geneBar 
   #plotly object to correct some issues with layout calculation by plotlyR and ggplot. 
   #As such this section relies on the consistency of plotly and ggplotly outputs. 
   #If plotly developers change the structure of the object or correct output the issues 
   #I am dealing with this section may have to be adjusted/removed 
   
   numberOfRows <- ceiling(geneNumber/plotcols)
   
   #relative X0 coordinates for each individual plot taken directly from a generated plotly object
   #X0s <- unique(sapply(seq.int(from=1,to=length(geneBar$x$layout$shapes),by=2),
   #                    function(y)geneBar$x$layout$shapes[[y]]$x0)) 
   
   #relative X1 coordinates for each individual plot taken directly from a generated plotly object
   #X1s <- unique(sapply(seq.int(from=1,to=length(geneBar$x$layout$shapes),by=2),
   #                     function(y)geneBar$x$layout$shapes[[y]]$x1))
   
   #relative Y0 coordinates for each individual plot taken directly from a generated plotly object
   Y0s <- unique(sapply(seq.int(from = 1, to = length(geneBar$x$layout$shapes), by = 2),
                        function(y) { geneBar$x$layout$shapes[[y]]$y0 })) 
   #relative Y1 coordinates for each individual plot taken directly from generated plotly object
   Y1s <- unique(sapply(seq_along(geneBar$x$layout$shapes), 
                        function(y) {
                            if(y%%2 == 1){
                                geneBar$x$layout$shapes[[y]]$y1
                            } else { geneBar$x$layout$shapes[[y-1]]$y1 }
                        }))
   
   #for some reason first and last rows of a plotly object are taller than other rows, 
   #same for columns - the difference is calculated here
   if(numberOfRows == 2){
       extraSpaceY = (0.5-(Y1s[1]-Y0s[1]))/1.5
   } else { extraSpaceY <- ((Y1s[1]-Y0s[1]) - (Y1s[2]-Y0s[2]))/1.5}
   
   #extraSpaceX <- (X1s[1]-X0s[1]) - (X1s[2]-X0s[2])
   #I can calculate it (see above) but it is defined by ggplot and as panel.spacing.x 
   # has never being changed it is same
   extraSpaceX <- 0.0164378008332743 
   #for all plotcol values
   
   #Recalculate all X Y relative positions to deal with an error in margin calculation 
   #for the first/last rows/column it also provide a more controled layout for barplots
   newX0 <- seq.int(from=0,to=1,by=1/plotcols)+extraSpaceX  
   newX0 <- rep(newX0[-length(newX0)],numberOfRows)
   newX1 <- seq.int(from=0,to=1,by=1/plotcols)-extraSpaceX
   newX1 <- rep(newX1[-1],numberOfRows)
   
   if(numberOfRows == 1){newY0=rep(0,plotcols);newY1=rep(1,plotcols)
   } else { newY0 <- seq.int(from=1,to=0,by=-1/numberOfRows)+extraSpaceY
            newY0 <- newY0[-1] 
            newY0 <- c(sapply(1:length(newY0),function(y)rep(newY0[y],plotcols)))
            newY1 <- seq.int(from=1,to=0,by=-1/numberOfRows)-extraSpaceY
            newY1 <- newY1[-length(newY1)]
            newY1 <- c(sapply(1:length(newY1),function(y)rep(newY1[y],plotcols)))
       }
   
  #index for all all xaxix elements in geneBar$x$layout
  #yaxisIndex is xaxisIndex +1 - no need to calculate it separately
  xaxisIndex <- grep("xaxis",names(geneBar$x$layout))
  
  sapply(1:length(xaxisIndex),function(y){ geneBar$x$layout[[xaxisIndex[y]]]$domain[1]<<- newX0[y]
                                           geneBar$x$layout[[xaxisIndex[y]]]$domain[2]<<- newX1[y]
                                           geneBar$x$layout[[xaxisIndex[y]+1]]$domain[1]<<- newY0[y]
                                           geneBar$x$layout[[xaxisIndex[y]+1]]$domain[2]<<- newY1[y]
                                         }) 
  #$Shapes basically - get rid of them.
  #This get rid of boxes around gene names
  #NB! use strip.background = element_blank() in geneBar theme definition makes boxes 
  #invisible but does not remove element descriptions
  #So the SIZE of output plotly object remains the same whether you see boxes or not. 
  #These two lines below actually makes plotly object MUCH smaller 
  boxIndex <- seq.int(from=1, to=length(geneBar$x$layout$shapes),by=1)
  geneBar$x$layout$shapes <- geneBar$x$layout$shapes[-boxIndex]
  
  #$Annotations - move gene names to proper X positions
  #First element in annotation contains info on Y axe label, the rest are gene labels
  #sanity check (length(geneBar$x$annotation)-1) == geneNumber
  sapply(seq.int(from=2, to=length(geneBar$x$layout$annotations)),
         function(y){geneBar$x$layout$annotations[[y]]$x <<- (newX0[y-1]+newX1[y-1])/2
                     geneBar$x$layout$annotations[[y]]$y <<- newY1[y-1]})
  
   return(geneBar)
}

#--------------
# FUNCTION
#    gtGene3
# COMMENT
#    
# REQUIRES
#    dplyr, reshape2, ggplot2, scales
# VARIABLES
#    edgeRObject - the output from the getDE() function
#    glist - a vector of gene names
#    gtable - a dataframe with gene expression in columns. In this context it takes data 
#             from out_list$rawCount or out_list$transformedCount
#    ytitle - Y-axis title - is not currently in use
#    ptitle - plot title
#    byGroup = "All" - a vector of group names. NB because in thiss app group names are 
#              passed from the selection box the function does not check whether
#              groups actually exist. For a standalone function this check needs to be added
#    scaleYes = T , if scaleYes=T the Y axis will be shared for all subplots           
#    colorPal - a color palette 
#    range - "nonnegative" | "normal" | "tozero" this is passed to rangemode attribute of yaxis (plotly) 
#            and used to improve subplot alignment
#    plotcols = 5 - pased to subplot(widths=) to shrink the width of an individual bargraph 
#               when the number of genes <= plotcols 
#    ytitle = "" goes into yaxix = list(title=) of a plotly layout
# RETURNS
#    a list of plotly objects (or a plotly plot - see comments)
gtGene3 <- function(edgeRObject = myDEOutput,
                    glist = NULL, 
                    gtable = NULL, 
                    byGroup = "All", 
                    ptitle = "", 
                    ylabel = "", 
                    scaleYes = T,
                    colorPal = npgColors,
                    range = "nonnegative",
                    plotcols=5){
    require(dplyr)
    require(reshape2)
    require(ggplot2)
    require(scales)
    
    if(length(unique(edgeRObject$dgeList$samples$group)) > length(colorPal)){
        reScaleColors <- colorPal
    } else {
        reScaleColors <- colorRampPalette(colorPal)(length(unique(edgeRObject$dgeList$samples$group)))
    }
    
    gtable$gene_name <-  toupper(gtable$gene_name)
    lookup <- data.frame(sample = rownames(edgeRObject$dgeList$samples), 
                         group = edgeRObject$dgeList$samples$group)
    
    mat_melt <- 
        gtable[gtable$gene_name %in% glist, ] %>% 
        melt() %>% 
        mutate(., Group = factor(lookup$group[match(variable, lookup$sample)], 
                                 levels = levels(edgeRObject$dgeList$samples$group)),
               gene_name = as.factor(gene_name))  
    
    if(!("All" %in% byGroup)){
        mat_melt <-  droplevels(mat_melt[mat_melt$Group %in% byGroup,])
    }
    
    mat_melt_s <- 
        group_by(mat_melt, gene_name, Group) %>% 
        summarize(., sd = sd(value, na.rm = T), value = mean(value))
    
    ngroups <- length(unique(mat_melt_s$Group))
    #plotlyOne is a function to create a barplot for ONE gene over all groups
    #it uses column names from mat_melt_s and data points from mat_melt
    
    if(length(unique(mat_melt_s$gene_name)) <= plotcols){  
        plotrows=1                                 
        colwidth=rep(1/plotcols,length(unique(mat_melt_s$gene_name)))
        
    } else {
        plotrows <- mat_melt_s$gene_name %>% unique %>% length %>% divide_by(plotcols) %>% ceiling()
        colwidth <- NULL
    }
    
    plotlyOne <- function(t){   
        
                 plot_ly(data = t,
                         x = ~Group,
                         y = ~value,
                         type = "bar",
                         color = ~Group,
                         colors = colorPal,
                         #at 300dpi it is one inch tall and about 3.5 inches on 75 DPI monitor
                         height = 300*plotrows, 
                         #ngroups*300, #the assuption here is that we need about 
                         #  300 pixels per bar includin spacing and some spacing for Y-axis 
                         width = NULL, 
                         marker = list(line = list(color = 'rgb(0,0,0)',
                                                 width = 1)),
                         showlegend = FALSE,                          
                         error_y = ~list(array = sd,
                                         symmetric = F,
                                         color = '#000000',
                                         thickness = 1)) %>% 
                add_trace(data = mat_melt[mat_melt$gene_name == t$gene_name,],
                                 x = ~Group, 
                                 y = ~value,
                                 type = "scatter",
                                 mode = "markers",
                                 marker = list(size = 7),
                                 color = I("black")) %>%
                add_annotations(text = ~gene_name,
                                x = 0.5,
                                y = 1.04,
                                yref = "paper",
                                xref = "paper",
                                xanchor = "middle",
                                yanchor = "top",
                                showarrow = FALSE,
                                font = list(size = 15)) %>% 
            layout(
                  xaxis = list(showgrid = FALSE,
                               showline = TRUE,
                               tickangle = 45),
                  yaxis = list(showgrid = FALSE,
                               showline = TRUE,
                               visible = TRUE,
                               rangemode = range,
                               pad = 0,
                               title = ylabel,
                               ticks = "outside",
                               showticklabels = TRUE))
                 
    }
    # when there are few plots a plot takes up an entire screen.
    # colwidth is used to decreases the width of idividual plot to 1/plotcols*100% of available screen width (17% by default)
    # and is used by sublot(). For a large number of plots it is not needed
    # NB the way the number of columns is determined by plotly::subplot() is screwed up and needs to be fixed at the sourse
    
    mat_melt_s %>% 
       split(.$gene_name) %>% 
       lapply(plotlyOne) -> plotlyOne_list
    
    if(length(plotlyOne_list) %/% plotcols != length(plotlyOne_list) / plotcols)
          {
           Ndummy <-  (length(plotlyOne_list) %/% plotcols+1)*plotcols - length(plotlyOne_list)
           plotlyDummy <- plotly_empty(
               
               type = "bar",
               #at 300dpi it is one inch tall and about 3.5 inches on 75 DPI monitor
               height =300*plotrows, 
               #the assuption here is that we need about 120 pixels per bar includin spacing and some spacing for Y-axis 
               width = NULL, 
               marker = list(line=list(color='rgb(0,0,0)',
                                       width=1)),
               showlegend = FALSE                          
               )
           
           for (i in seq(length(plotlyOne_list)+1,length(plotlyOne_list)+Ndummy,by=1)){
               
               plotlyOne_list[[i]] <- plotlyDummy}
        } else {}
       
      
        subplot(plotlyOne_list, margin = c(0,0.07,0.1,0),nrows=plotrows,  
               shareY=scaleYes,titleY = T,which_layout = c(1)) %>% 
        layout(margin = list(t = 50),
               xaxis = list(showgrid = FALSE,
                            showline = TRUE,
                            tickangle = 45),
               yaxis = list(showgrid = FALSE,
                            showline = TRUE,
                            visible = TRUE,
                            rangemode = range,
                            pad=10,
                            title=ylabel,
                            ticks = "outside",
                            showticklabels = TRUE))
}


#--------------
# FUNCTION
#    customValueBox
# COMMENT
#    Clickable InfoBox
#    stolen from https://stackoverflow.com/questions/37169039/direct-link-to-tabitem-with-r-shiny-dashboard
# REQUIRES
#    shiny
# VARIABLES
#    title, 
#    tab = NULL, 
#    value = NULL, 
#    subtitle = "", 
#    color = "navy", 
#    width = 4, 
#    href = NULL, 
#    fill = FALSE
# RETURNS
#    an HTML object??? idk
customValueBox <- function(title, 
                           tab = NULL, 
                           value = NULL, 
                           subtitle = "", 
                           color = "navy", 
                           width = 4, 
                           href = NULL, 
                           fill = FALSE) {
    #shinydashboard::validateColor(color)
    #shinydashboard::tagAssert(icon, type = "i")
    colorClass <- paste0("bg-", color)
    boxContent <- div(class = "value-box", 
                      class = if(fill){ colorClass }, 
                      onclick = if(!is.null(tab)) { 
                          paste0("$('.sidebar a')).filter(function() { return ($(this).attr('data-value') == ", 
                                 tab, 
                                 ")}).click()") 
                      },
                      #span(class = "value-box-icon", class = if (!fill) colorClass, icon),  
                      div(class = "value-box-content", 
                          style = "font-size: 20px; font-weight:bold",
                          span(class = "value-box-text", title), 
                          if(!is.null(value)){ 
                              span(class = "value-box-number", value) 
                          }, 
                          if(!is.null(subtitle)){ 
                              p(subtitle) 
                          }
                      )
    )
    if(!is.null(href)){ 
        boxContent <- a(href = href, boxContent) 
    }
    div(class = if(!is.null(width)){ paste0("col-sm-", width) }, boxContent)
}


#--------------
# FUNCTION
#    plotlyPathways
# COMMENT
#    plotly-ification of plotCIs from qusage ¯\_(ツ)_/¯
# REQUIRES
#    plotly
# VARIABLES
#    qusageObject = qusageObject generated by qusage2
#    pathway.subset = subset pathways in canonical pathways gmt
#    order.by = one of c("logFC", "PValue", "FDR")
#    signif.select = select significance by either c("PValue", "FDR")
#    signif.thresh = what is the significance threshold? default = 0.05
#    signif.only = should only significant results be plotted. default = FALSE
#    PDF = boolean: controls opacity of plot for PDF download. default = FALSE
#    ... = additional parameters passed to primary plot_ly() call
# RETURNS
#    a plotly bubble plot of pathways.
plotlyPathways <- function(qusageObject, 
                           pathway.subset = "ALL",
                           order.by = c("logFC","PValue"),
                           signif.select = c("PValue", "FDR"),
                           signif.thresh = 0.05,
                           signif.only = FALSE, 
                           PDF = FALSE, ...){
    stopifnot(length(order.by) == 1, length(signif.select) == 1)
    if(PDF){
        PDF <- 1
    } else { PDF <- 0.66 }
    pathwayDat <- 
        qusageObject$PathwayTable %>% # grab the pathway stats table
        .[complete.cases(.),] %>% # remove pathways that didn't have enough genes for stats
        .[order(.[order.by]),] %>% # reorder pathways by `order.by` variable
        tibble::rownames_to_column(., "Pathway") %>% # move rownames to first column 
        plyr::mutate(.,  
                     lowCI = abs(lowCI), # use absolute value for easier plotting
                     highCI = abs(highCI), 
                     # make new variable for user defined significance
                     signif = .[,signif.select, drop = T] < signif.thresh) %>% 
                     # you can pipe to pretty much anything if it's in brackets
                     # if subset is selected filter, otherwise return .
                     { if(pathway.subset != "ALL") .[grep(paste0("^", pathway.subset), .$Pathway),] else . } %>% 
                     { if(signif.only == TRUE) filter(., signif == TRUE) else . } 
    
    colBarPars <- list(thickness = 15,
                       len = 0.40,
                       title = signif.select,
                       tickmode = "auto") #, #dtick = 0.2 
                       #tickvals = seq(0,1,0.1),
                       #ticktext = seq(0,1,0.1))
    
    # for consistent colouring we only color by p-value so the range is 0 to 1
    fdrPColorBins <- seq(0, 1, length = 256) 
    # generate a grey to red color pallette
    fdrPColorPal <- brewer.pal(11, "RdBu")[1:6] %>% rev(.) %>% colorRampPalette(.)
    # extend color pallette to the same size as colorBins
    names(fdrPColorBins) <- fdrPColorPal(256)
    # plotlyColors
    plotlyColors <- names(fdrPColorBins)[findInterval(pathwayDat[,signif.select], fdrPColorBins, all.inside = T)]
    
    pathwayPlot <- 
        pathwayDat %>%
        plot_ly(x = 1:nrow(.),
                y = ~logFC, 
                key = ~Pathway,
                hoverinfo = 'text',
                text = paste("Pathway: ", .$Pathway, "\n", 
                             "Size: ", .$size, "\n",
                             "logFC: ", round(.$logFC, 3), "\n", 
                             paste0(signif.select, ": "), 
                             formatC(.[,signif.select], format = "e", 1)),
                type = 'scatter', 
                mode = 'markers',
                marker = list(symbol = 'circle', 
                              opacity = PDF,
                              size = rescale(.$size, to = c(10,75)),
                              sizemode = 'diameter',
                              line = list(width = 1, color = "#FFFFFF"),
                              color = formula(paste("~", signif.select)), 
                              colors = plotlyColors,
                              # colorscale = "RdBu",
                              reversescale = T,
                              colorbar = colBarPars), 
                ...) %>% 
        layout(xaxis = list(showticklabels = F,
                            showgrid = F,
                            zeroline = F),
               yaxis = list(title = "Pathway LogFC"),
               title = "<b>QuSAGE Pathway Analysis</b>") %>% 
        config(modeBarButtonsToRemove = list("pan2d", "lasso2d", "zoomIn2d", "zoomOut2d", 
                                             "autoScale2d", "hoverCompareCartesian"),
               displaylogo = FALSE)
    pathwayPlot
}

#--------------
# FUNCTION
#    filterPathwayDownloadTable
# COMMENT
#    this is a companion to plotlyPathways. It generates a downloadable table corresponding
#    to the plotlyPathways plot.
# REQUIRES
#    tibble, plyr
# VARIABLES
#    qusageObject = qusageObject generated by qusage2
#    pathway.subset = subset pathways in canonical pathways gmt
#    order.by = one of c("logFC", "PValue", "FDR")
#    signif.select = select significance by either c("PValue", "FDR")
#    signif.thresh = what is the significance threshold? default = 0.05
#    signif.only = should only significant results be plotted. default = FALSE
# RETURNS
#    pathwayDataAndGenes -- a table of data used for the plotlyPathways plot which can now be downloaded
filterPathwayDownloadTable <- function(qusageObject, 
                                       pathway.subset = "ALL",
                                       order.by = c("logFC","PValue"),
                                       signif.select = c("PValue", "FDR"),
                                       signif.thresh = 0.05,
                                       signif.only = FALSE, ...){
    stopifnot(length(order.by) == 1, length(signif.select) == 1)
    pathwayDat <- 
        qusageObject$PathwayTable %>% # grab the pathway stats table
        #.[complete.cases(qusageObject$PathwayTable),] %>% # remove pathways that didn't have enough genes for stats
        #.[order(.[order.by]),] %>% # reorder pathways by `order.by` variable
        tibble::rownames_to_column(., "Pathway") %>% # move rownames to column
        plyr::mutate(.,  
                     lowCI = abs(lowCI), # use absolute value for easier plotting
                     highCI = abs(highCI), 
                     # make new variable for user defined significance
                     signif = .[,signif.select, drop = T] < signif.thresh) %>% 
        # you can pipe to pretty much anything if it's in brackets
        # if subset is selected filter for that using grep, otherwise return .
                     { if(pathway.subset != "ALL") .[grep(paste0("^", pathway.subset), .$Pathway),] else . } %>% 
                     { if(signif.only == TRUE) filter(., signif == TRUE) else . } # same as above but with signif
    # this is a fucking nightmare of a function
    # get the indexes of genes in selected pathways
    stopifnot(nrow(pathwayDat) > 0)
    pathwayDataAndGenes <- 
        qusageObject$PathwayIndexs[names(qusageObject$PathwayIndexs) %in% pathwayDat$Pathway] %>% 
        # convert indexes to gene names and then to a comma separated string
        lapply(., function(x){ toString(rownames(qusageObject$GeneTable[x,]))}) %>% 
        unlist %>% 
        data.frame %>% 
        set_colnames(., "Genes") %>% 
        cbind(pathwayDat, .) %>% 
        mutate(., signif = NULL) %>% 
        .[order(.[order.by]),]
    
    return(pathwayDataAndGenes)
}

#--------------
# FUNCTION
#    plotlyPathwayGenes
# COMMENT
#    plotly-ification of plotCIsGenes from qusage ¯\_(ツ)_/¯
# REQUIRES
#    plotly
# VARIABLES
#    qusageObject = qusageObject generated by qusage2
#    pathwayName = name of pathway to plot
#    color.points = color for points in plot
# RETURNS
#    pathGenesPlot -- a plot of genes from a pathway. Companion to plotlyPathways 
plotlyPathwayGenes <- function(qusageObject,
                               pathwayName = NULL,
                               color.points = "#313695", ...){
    pathGenesPlot <- 
        qusageObject$PathwayIndexs[[pathwayName]] %>% 
        qusageObject$GeneTable[.,] %>% 
        mutate(., name = rownames(.)) %>%
        .[order(.$mean),] %>% 
        mutate(., tickpos = 1:nrow(.))
    
    # if zero is not in the data range, 
    maxRange <- range(rowSums(pathGenesPlot[,c("mean", "sd")]))
    minRange <- range(apply(pathGenesPlot[,c("mean", "sd")], 1, function(x){x[1] - x[2]}))
    currentRange <- c(min(minRange, maxRange), max(minRange, maxRange))
    if(!between(0, currentRange[1], currentRange[2])){
        plotRange <- "tozero"
    } else { plotRange <- "normal" }
    
    pathGenesPlot %<>% 
    plot_ly(data = ., 
            x = 1:nrow(.),
            y = ~mean,
            hoverinfo = 'text',
            text = paste("Gene Name: ", .$name, "\n",
                         "logFC: ", round(.$mean, 3), "\n", 
                         sep = ""),
            type = 'scatter', 
            mode = 'markers',
            marker = list(size = 10, 
                          color = color.points),
            error_y = list(type = "data",
                           array = ~sd,
                           color = "#252525"),
            ...) %>% 
    layout(title = pathwayName,
           xaxis = list(showticklabels = T,
                        tickmode = "array", 
                        tickvals = ~tickpos, 
                        ticktext = ~name,
                        showgrid = F,
                        zeroline = F),
           yaxis = list(title = "Gene LogFC", 
                        zeroline = T, 
                        rangemode = plotRange)) %>% 
    config(modeBarButtonsToRemove = list("pan2d", "lasso2d", "zoomIn2d", "zoomOut2d", 
                                         "autoScale2d", "hoverCompareCartesian"),
           displaylogo = FALSE)
    pathGenesPlot
}


#--------------
# FUNCTION
#    heatmapr2 is heatmaply with better checks for col_side_colors
# COMMENT
#    this should be added to the feature requests of talgalili/heatmaply
# REQUIRES
#    heatmaply, dendextend, seriation, assertthat
# VARIABLES
#    all the same as heatmapr
# RETURNS
#    a heatmapr object to be passed to heatmaply

heatmapr2 <- function (x, Rowv, Colv, distfun = dist, hclustfun = hclust, 
                       dist_method = NULL, hclust_method = NULL, distfun_row, hclustfun_row, 
                       distfun_col, hclustfun_col, dendrogram = c("both", "row", "column", "none"), 
                       reorderfun = function(d, w) reorder(d, w), k_row = 1, k_col = 1, symm = FALSE, 
                       revC, scale = c("none", "row", "column"), na.rm = TRUE, labRow = rownames(x), 
                       labCol = colnames(x), cexRow, cexCol, digits = 3L, cellnote = NULL, 
                       theme = NULL, colors = "RdYlBu", width = NULL, height = NULL, 
                       xaxis_height = 80, yaxis_width = 120, xaxis_font_size = NULL, 
                       yaxis_font_size = NULL, brush_color = "#0000FF", show_grid = TRUE, 
                       anim_duration = 500, row_side_colors = NULL, col_side_colors = NULL, 
                       seriate = c("OLO", "mean", "none", "GW"), point_size_mat = NULL, 
                       custom_hovertext = NULL, ...) 
{
    require(heatmaply, quietly = T); require(dendextend, quietly = T); require(seriation, quietly = T)
    require(assertthat, quietly = T)
    distfun <- match.fun(distfun)
    if (!is.null(dist_method)) {
        distfun_old <- distfun
        distfun <- function(x) {
            distfun_old(x, method = dist_method)
        }
    }
    if (!is.null(hclust_method)) {
        hclustfun_old <- hclustfun
        hclustfun <- function(x) {
            hclustfun_old(x, method = hclust_method)
        }
    }
    if (missing(distfun_row)) 
        distfun_row <- distfun
    if (missing(hclustfun_row)) 
        hclustfun_row <- hclustfun
    if (missing(distfun_col)) 
        distfun_col <- distfun
    if (missing(hclustfun_col)) 
        hclustfun_col <- hclustfun
    distfun_row <- match.fun(distfun_row)
    distfun_col <- match.fun(distfun_col)
    if (!is.matrix(x)) {
        x <- as.matrix(x)
    }
    if (!is.matrix(x)) 
        stop("x must be a matrix")
    seriate <- match.arg(seriate)
    nr <- nrow(x)
    nc <- ncol(x)
    rownames(x) <- labRow %||% as.character(1:nrow(x))
    colnames(x) <- labCol %||% as.character(1:ncol(x))
    rownames(x) <- heatmaply:::fix_not_all_unique(rownames(x))
    colnames(x) <- heatmaply:::fix_not_all_unique(colnames(x))
    if (!missing(cexRow)) {
        if (is.numeric(cexRow)) {
            xaxis_font_size <- cexRow * 14
        }
        else {
            warning("cexRow is not numeric. It is ignored")
        }
    }
    if (!missing(cexCol)) {
        if (is.numeric(cexCol)) {
            yaxis_font_size <- cexCol * 14
        }
        else {
            warning("cexCol is not numeric. It is ignored")
        }
    }
    scale <- match.arg(scale)
    if (scale == "row") {
        x <- sweep(x, 1, rowMeans(x, na.rm = na.rm))
        x <- sweep(x, 1, apply(x, 1, sd, na.rm = na.rm), "/")
    }
    else if (scale == "column") {
        x <- sweep(x, 2, colMeans(x, na.rm = na.rm))
        x <- sweep(x, 2, apply(x, 2, sd, na.rm = na.rm), "/")
    }
    dendrogram <- match.arg(dendrogram)
    if (missing(Rowv)) {
        Rowv <- dendrogram %in% c("both", "row")
    }
    if (missing(Colv)) {
        if (dendrogram %in% c("both", "column")) {
            Colv <- if (symm) 
                "Rowv"
            else TRUE
        }
        else {
            Colv <- FALSE
        }
    }
    if (isTRUE(Rowv)) {
        Rowv <- switch(seriate, 
                       mean = rowMeans(x, na.rm = na.rm), 
                       none = 1:nrow(x), OLO = {
                           dist_x <- distfun_row(x)
                           hc_x <- hclustfun_row(dist_x)
                           dend_x <- as.dendrogram(hc_x)
                           dend_x2 <- seriate_dendrogram(dend_x, dist_x, 
                                                         method = "OLO")
                           dend_x2
                       }, GW = {
                           dist_x <- distfun_row(x)
                           hc_x <- hclustfun_row(dist_x)
                           dend_x <- as.dendrogram(hc_x)
                           dend_x2 <- seriate_dendrogram(dend_x, dist_x, 
                                                         method = "GW")
                           dend_x2
                       })
    }
    if (is.numeric(Rowv)) {
        Rowv <- reorderfun(as.dendrogram(hclustfun_row(distfun_row(x))), 
                           Rowv)
        Rowv <- rev(Rowv)
    }
    if (is.hclust(Rowv)) 
        Rowv <- as.dendrogram(Rowv)
    if (is.dendrogram(Rowv)) {
        rowInd <- order.dendrogram(Rowv)
        if (nr != length(rowInd)) {
            stop("Row dendrogram is the wrong size")
        }
    }
    else {
        if (!is.null(Rowv) && !is.na(Rowv) && !identical(Rowv, 
                                                         FALSE)) {
            warning("Invalid value for Rowv, ignoring")
        }
        Rowv <- NULL
        rowInd <- 1:nr
    }
    Rowv <- rev(Rowv)
    rowInd <- rev(rowInd)
    if (identical(Colv, "Rowv")) {
        Colv <- Rowv
    }
    if (isTRUE(Colv)) {
        Colv <- switch(seriate, mean = colMeans(x, na.rm = na.rm), 
                       none = 1:ncol(x), OLO = {
                           dist_x <- distfun_col(t(x))
                           hc_x <- hclustfun_col(dist_x)
                           o <- seriate(dist_x, method = "OLO", control = list(hclust = hc_x))
                           dend_x <- as.dendrogram(hc_x)
                           dend_x2 <- rotate(dend_x, order = rev(labels(dist_x)[get_order(o)]))
                           dend_x2
                       }, GW = {
                           dist_x <- distfun_col(t(x))
                           hc_x <- hclustfun_col(dist_x)
                           o <- seriate(dist_x, method = "GW", control = list(hclust = hc_x))
                           dend_x <- as.dendrogram(hc_x)
                           dend_x2 <- rotate(dend_x, order = rev(labels(dist_x)[get_order(o)]))
                           dend_x2
                       })
    }
    if (is.numeric(Colv)) {
        Colv <- reorderfun(as.dendrogram(hclustfun_col(distfun_col(t(x)))), 
                           rev(Colv))
    }
    if (is.hclust(Colv)) 
        Colv <- as.dendrogram(Colv)
    if (is.dendrogram(Colv)) {
        Colv <- rev(Colv)
        colInd <- order.dendrogram(Colv)
        if (nc != length(colInd)) {
            stop("Col dendrogram is the wrong size")
        }
    }
    else {
        if (!is.null(Colv) && !is.na(Colv) && !identical(Colv, 
                                                         FALSE)) {
            warning("Invalid value for Colv, ignoring")
        }
        Colv <- NULL
        colInd <- 1:nc
    }
    if (missing(revC)) {
        if (symm) {
            revC <- TRUE
        }
        else if (is.dendrogram(Colv) & is.dendrogram(Rowv) & 
                 identical(Rowv, rev(Colv))) {
            revC <- TRUE
        }
        else {
            revC <- FALSE
        }
    }
    if (revC) {
        Colv <- rev(Colv)
        colInd <- rev(colInd)
    }
    if (is.null(cellnote)) 
        cellnote <- x
    if (is.numeric(cellnote)) {
        cellnote <- round(cellnote, digits = digits)
    }
    x <- x[rowInd, colInd, drop = FALSE]
    cellnote <- cellnote[rowInd, colInd, drop = FALSE]
    if (!is.null(point_size_mat)) {
        point_size_mat <- point_size_mat[rowInd, colInd, drop = FALSE]
    }
    if (!is.null(custom_hovertext)) {
        custom_hovertext <- custom_hovertext[rowInd, colInd, 
                                             drop = FALSE]
    }
    if (!is.null(row_side_colors)) {
        if (!(is.data.frame(row_side_colors) | is.matrix(row_side_colors))) {
            row_side_colors <- data.frame(row_side_colors = row_side_colors)
        }
        assert_that(nrow(row_side_colors) == nrow(x))
        row_side_colors <- row_side_colors[rowInd, , drop = FALSE]
    }
    if (!is.null(col_side_colors)) {
        if (!(is.data.frame(col_side_colors) | is.matrix(col_side_colors))) {
            col_side_colors <- data.frame(col_side_colors)
            colnames(col_side_colors) <- "col_side_colors"
        }
        assert_that(nrow(col_side_colors) == ncol(x))
        col_side_colors <- col_side_colors[colInd, , drop = FALSE]
    }
    dendextend::assign_dendextend_options()
    if (is.dendrogram(Rowv)) {
        if (is.na(k_row)) 
            k_row <- find_k(Rowv)$k
        if (k_row > 1) {
            # consider changing color-scheme for clusters
            Rowv <- color_branches(Rowv, k = k_row, col = heatmaply:::k_colors(k_row))
        }
    }
    if (is.dendrogram(Colv)) {
        if (is.na(k_col)) 
            k_col <- find_k(Colv)$k
        if (k_col > 1) {
            # consider changing color-scheme for clusters
            Colv <- color_branches(Colv, k = k_col, col = heatmaply:::k_colors(k_col))
        }
    }
    rowDend <- if (is.dendrogram(Rowv)) 
        Rowv
    else NULL
    colDend <- if (is.dendrogram(Colv)) 
        Colv
    else NULL
    if (!is.null(cellnote)) {
        if (is.null(dim(cellnote))) {
            if (length(cellnote) != nr * nc) {
                stop("Incorrect number of cellnote values")
            }
            dim(cellnote) <- dim(x)
        }
        if (!identical(dim(x), dim(cellnote))) {
            stop("cellnote matrix must have same dimensions as x")
        }
    }
    if (is.data.frame(custom_hovertext)) {
        custom_hovertext <- as.matrix(custom_hovertext)
    }
    mtx <- list(data = as.matrix(x), cellnote = cellnote, dim = dim(x), 
                rows = rownames(x), cols = colnames(x), point_size_mat = point_size_mat, 
                custom_hovertext = custom_hovertext)
    if (is.factor(x)) {
        colors <- col_factor(colors, x, na.color = "transparent")
    }
    else {
        rng <- range(x, na.rm = TRUE)
        if (scale %in% c("row", "column")) {
            rng <- c(max(abs(rng)), -max(abs(rng)))
        }
        colors <- col_numeric(colors, rng, na.color = "transparent")
    }
    options <- NULL
    options <- c(options, list(xaxis_height = xaxis_height, yaxis_width = yaxis_width, 
                               xaxis_font_size = xaxis_font_size, yaxis_font_size = yaxis_font_size, 
                               brush_color = brush_color, show_grid = show_grid, anim_duration = anim_duration))
    if (is.null(rowDend)) {
        c(options, list(yclust_width = 0))
    }
    if (is.null(colDend)) {
        c(options, list(xclust_height = 0))
    }
    heatmapr <- list(rows = rowDend, cols = colDend, matrix = mtx, 
                     theme = theme, options = options, cellnote = cellnote)
    if (!is.null(row_side_colors)) 
        heatmapr[["row_side_colors"]] <- row_side_colors
    if (!is.null(col_side_colors)) 
        heatmapr[["col_side_colors"]] <- col_side_colors
    class(heatmapr) <- "heatmapr"
    heatmapr
}


#--------------
# FUNCTION
#    plotlyPDF
# COMMENT
#    this function is a local alternative to orca() <- cause this costs money
#    install_phantomjs is commented out because it is finicky on non-linux systems
# REQUIRES
#    webshot
#    htmlwidgets
# VARIABLES
#    p = the plotly plot
#    name = the filename to save to (exclude file extension)
#    delay = how long to wait (in seconds) for plot to render before generating pdf
#    ... = any variables passed to webshot
# RETURNS
#    a pdf and html of the plotly plot
plotlyPDF <- function(p, name = "name", delay = 5, ...){
    require(webshot); require(htmlwidgets)
    #if(!file.exists("~/bin/phantomjs")){
    #    webshot::install_phantomjs()
    #}
    saveWidget(p, paste0(name, ".html"), selfcontained = TRUE)
    webshot(url = paste0(name, ".html"), file = paste0(name, ".pdf"), delay = delay, ...)
    
}

#--------------
# FUNCTION
#    listIntersect
# COMMENT
#    this is a helper function for venn.diagram2. it generates all the sets needed
#    for the draw.x.venn functions
# REQUIRES
#    magrittr
# VARIABLES
#    x = a list (possibly named) of genes (or anything that can be matched) for being passed
#    to venn.diagram2
# RETURNS
#    all possible intersections of list elements
listIntersect <- function(x){
    require(magrittr)
    if(length(x) >= 2 && length(x) <= 5){
        modes <- vector(mode = "list", length = length(x) - 2)
        for(i in length(x):2){
            modes[[i-1]] <- t(combn(length(x), i))
        }
    } else if(length(x) == 1){
        return(length(x[[1]]))
    } else {
        stop("Inappropriate number of sets to intersect")
    }
    
    out <- 
        lapply(modes, nrow) %>% 
        unlist() %>% 
        sum() %>% 
        vector(mode = "list", length = .)
    
    k <- 1
    for(i in 1:length(modes)){
        for(j in 1:nrow(modes[[i]])){
            out[[k]] <- Reduce(intersect, x[modes[[i]][j,]])
            names(out)[k] <- paste("n", paste(modes[[i]][j,], collapse = ""), sep = "")
            k <- k+1
        }
    }
    return(out)
}

#--------------
# FUNCTION
#    venn.diagram2
# COMMENT
#    this is a replacement for the original venn.diagram function from VennDiagram package
# REQUIRES
#    VennDiagram
#    dplyr
#    tibble
#    ggplot2
# VARIABLES
#    all arguments are the same as in the original venn.diagram. 
#    NB! This function was tested only with named list as the main argument
# RETURNS
#    listOut <- list (ggplotVenn     =  plotPoly,
#                     vennPartitions = vennPartitions)
#    $ggplotVenn is a ggplot of a Venn diagram
#    $vennPartitions is a list of elements in each Venn diagram subset   
venn.diagram2 <- function (x, main = NULL, sub = NULL, main.pos = c(0.5, 1.05), main.fontface = "plain", 
                           main.fontfamily = "serif", main.col = "black", main.cex = 1, 
                           main.just = c(0.5, 1), sub.pos = c(0.5, 1.05), sub.fontface = "plain", 
                           sub.fontfamily = "serif", sub.col = "black", sub.cex = 1, 
                           sub.just = c(0.5, 1), category.names = names(x),
                           print.mode = "raw", sigdigs = 3, direct.area = FALSE, area.vector = 0, 
                           total.population = NULL, lower.tail = TRUE,saveSubset =FALSE, 
                           fillCircle=c("#E64B35FF", "#4DBBD5FF", "#00A087FF", "#3C5488FF", "#F39B7FFF"), 
                           ...) {
    #dependencies intoduced  by me
    require(VennDiagram); require(dplyr); require(tibble); require(ggplot2)
    
    if (0 == length(x) | length(x) > 5) {
        stop("Incorrect number of elements.", call. = FALSE)
    }

    # start re-parameterized venn
    # NAs are automatically removed and all elements are unique cause why the fuck not
    for (i in 1:length(x)) {
        x[[i]] <- x[[i]][!is.na(x[[i]])] # remove NAs
        x[[i]] <- unique(x[[i]]) # make sure all unique
    }
    # when the first argument of switch evaluates to a number 
    # switch returns the argument in that position (no need for "arg" = { to.eval })
    switch(length(x),
           grob.list <- draw.single.venn(area = length(x[[1]]), 
                                         category = category.names, 
                                         ind = FALSE, ...),
           grob.list <- draw.pairwise.venn(area1 = length(x[[1]]), area2 = length(x[[2]]), 
                                           cross.area = length(intersect(x[[1]], x[[2]])), 
                                           category = category.names, ind = FALSE, 
                                           cat.dist = 0.075,
                                           print.mode = print.mode, sigdigs = sigdigs, ...),
           {y <- listIntersect(x)
           grob.list <- draw.triple.venn(area1 = length(x[[1]]), area2 = length(x[[2]]),
                                         area3 = length(x[[3]]),
                                         n12 = length(y[["n12"]]), n13 = length(y[["n13"]]),
                                         n23 = length(y[["n23"]]), n123 = length(y[["n123"]]),
                                         category = category.names, ind = FALSE, 
                                         cat.dist = c(0.075, 0.075, 0.05),
                                         print.mode = print.mode, sigdigs = sigdigs, ...)
           },
           {y <- listIntersect(x)
           grob.list <- draw.quad.venn(area1 = length(x[[1]]), area2 = length(x[[2]]), 
                                       area3 = length(x[[3]]), area4 = length(x[[4]]), 
                                       n12 = length(y[["n12"]]), n13 = length(y[["n13"]]), 
                                       n14 = length(y[["n14"]]), n23 = length(y[["n23"]]), 
                                       n24 = length(y[["n24"]]), n34 = length(y[["n34"]]), 
                                       n123 = length(y[["n123"]]), n124 = length(y[["n124"]]), 
                                       n134 = length(y[["n134"]]), n234 = length(y[["n234"]]), 
                                       n1234 = length(y[["n1234"]]), category = category.names, 
                                       cat.dist = c(0.25, 0.25, 0.125, 0.125),
                                       ind = FALSE, print.mode = print.mode, sigdigs = sigdigs, ...)
           },
           {y <- listIntersect(x)
           grob.list <- draw.quintuple.venn(area1 = length(x[[1]]), area2 = length(x[[2]]), 
                                            area3 = length(x[[3]]), area4 = length(x[[4]]), 
                                            area5 = length(x[[5]]), 
                                            n12 = length(y[["n12"]]), n13 = length(y[["n13"]]), 
                                            n14 = length(y[["n14"]]), n23 = length(y[["n23"]]), 
                                            n24 = length(y[["n24"]]), n34 = length(y[["n34"]]), 
                                            n15 = length(y[["n15"]]), n25 = length(y[["n25"]]), 
                                            n35 = length(y[["n35"]]), n45 = length(y[["n45"]]),
                                            n123 = length(y[["n123"]]), n124 = length(y[["n124"]]), 
                                            n134 = length(y[["n134"]]), n234 = length(y[["n234"]]), 
                                            n125 = length(y[["n125"]]), n135 = length(y[["n135"]]), 
                                            n145 = length(y[["n145"]]), n235 = length(y[["n235"]]), 
                                            n245 = length(y[["n245"]]), n345 = length(y[["n345"]]),
                                            n1234 = length(y[["n1234"]]), n1235 = length(y[["n1235"]]), 
                                            n1245 = length(y[["n1245"]]), n1345 = length(y[["n1345"]]), 
                                            n2345 = length(y[["n2345"]]), n12345 = length(y[["n12345"]]), 
                                            category = category.names, ind = FALSE, print.mode = print.mode, 
                                            cat.dist = rep(0.275, 5), sigdigs = sigdigs, ...)
           }
    )

    if (!is.null(sub)) {
        grob.list <- add.title(gList = grob.list, x = sub, pos = sub.pos, 
                               fontface = sub.fontface, fontfamily = sub.fontfamily, 
                               col = sub.col, cex = sub.cex)
    }
    if (!is.null(main)) {
        grob.list <- add.title(gList = grob.list, x = main, pos = main.pos, 
                               fontface = main.fontface, fontfamily = main.fontfamily, 
                               col = main.col, cex = main.cex)
    }

    #capture grob.list and convert it into a ggplot output
    
    #structure of grob.list object
    #let conLength be the number of sets
    #1:(conLenght*2) - coordinated of ellipces/circles for each set repeated twice (ones for countour, once for fill)
    #order coresponds to the order of set labels from x
    #(vennLength - conLength+1):vennLength - set labels in $lebel, label coordinates in $x adn $y
    #conLength*2+1:(vennLength-conLength) there is is list of labeles for subsets and, 
    #  sometimes, GRID.line object for a pointer
    #lines to a very small subsets. Information for these GRID.lines need to be captured and separated from subset labels
    vennLength <- length(grob.list)
    conLength <- length(x)
    coorPoly <- lapply(grob.list[1:conLength], function(t){ data.frame(x = unclass(t$x), y = unclass(t$y)) })
    names(coorPoly) <- sapply(grob.list[(vennLength-conLength+1):vennLength], function(t){ unclass(t$label) })
    
    #this is to correct a discrepancy in coordintes vs set order when the number of sets is four
    if(conLength == 4){ 
        names(coorPoly) <- c(names(coorPoly)[2], names(coorPoly)[1], 
                             names(coorPoly)[4], names(coorPoly)[3])
    } 
    coorPoly <- dplyr::bind_rows(coorPoly,.id="set")# set coordinates
    label.grob.list <- grob.list[(conLength*2+1):vennLength]
    
    # there might be lines here when the subset are too small - they need to be removed and 
    # later used in a ggplot object .... oh, bother.
    setLabel <- dplyr::bind_rows(lapply(grob.list[(vennLength-conLength+1):vennLength],
                                        function(t) {
                                            data.frame(x = unclass(t$x), y = unclass(t$y),
                                                       label = t$label, stringsAsFactors = F)
                                        }))
    
    
    vennLabel <- dplyr::bind_rows(lapply(grob.list[(conLength*2+1):(vennLength-conLength)], 
                                         function(t) { 
                                             if(!grepl("GRID.lines",t$name)) { 
                                                 data.frame(x = unclass(t$x), y = unclass(t$y),
                                                            label = t$label, stringsAsFactors = F)
                                             }
                                         }))
    
    segments = vennLength - (nrow(vennLabel)+nrow(setLabel)+conLength*2) #
    
    if(segments !=0){
        coorSegment <- dplyr::bind_rows(lapply(grob.list[1:vennLength], 
                                               function(t) {
                                                   if(grepl("GRID.lines", t$name)) { 
                                                       matrix(c(unclass(t$x), unclass(t$y)), nrow=1) %>% 
                                                           data.frame %>% 
                                                           set_colnames(., c("x0","xend","y0","yend"))
                                                   }
                                               }))
        
        coorSegment$y0 <- coorSegment$y0 - 0.035 
    }
    
    #when N=2 Venn Diagramm author changes the order of sets in a grob relative to the entered order of sets. 
    # the largest set becomes the first I shudder to think what heppens if they are equal :)
    # vennLabel$label <- paste0("a",1:nrow(vennLabel),"\n",vennLabel$label)
    # 
    # if((conLength == 2) & length(x[[2]]) > length(x[[1]])) {
    #     vennLabel$label[1] <- gsub("a1","a2", vennLabel$label[1])
    #     vennLabel$label[2] <- gsub("a2","a1", vennLabel$label[2]) 
    # }
    
    plotPoly <- ggplot(coorPoly) + theme_classic() + 
        geom_polygon(mapping = aes(x = x, y = y, group = set, fill = set, color = set), alpha = 0.4) + 
        coord_fixed(xlim = c(0, 1), ylim = c(0, 1)) +
        scale_fill_manual(values = fillCircle) +
        {if (exists("coorSegment")){
            geom_segment(data = coorSegment, aes(x = x0, y = y0, xend = xend, yend = yend))
        }} +
        annotate(geom = "text", x = vennLabel$x, y = vennLabel$y, label = vennLabel$label, size = 4) +
        annotate(geom = "text", x = setLabel$x, y = setLabel$y, label = setLabel$label, fontface = 2, size = 4) +
        theme(axis.line = element_blank(), axis.text.x = element_blank(),
              axis.text.y = element_blank(), axis.ticks = element_blank(),
              axis.title.x = element_blank(), axis.title.y = element_blank(), 
              legend.position = "none", plot.margin = unit(c(1,1,1,1), "cm"))
    #set names in this list a1....aN corresponds to the order of subsets in grob.list objects
    vennPartitions <- calculate.overlap2(x) 
    
    vennDF <- getSetTable(x)    

    listOut <- list (ggplotVenn = plotPoly, vennPartitions = vennPartitions, vennDF = vennDF)  
}

#' Function that extract elements of each subset
#' 
#' @param set a named list of sets to be intersected
#' 
getSetTable <- function(set){
    
    VennDF <- get.venn.partitions(set)
    
    vennDFTerms <- 
        VennDF %>% 
        .[,c(ncol(.), 1:(ncol(.)-1))] %>% 
        dplyr::select(Counts = '..count..', dplyr::everything(), -c('..set..', '..values..'))
    
    vennDF <- 
        sapply(X = VennDF$..values.., FUN = toString) %>% 
        cbind(vennDFTerms, Genes = .)
    
    return(vennDF)
}


#--------------
# FUNCTION
#    this is to replace calclulate.overlap() from VennDiagram package. 
# COMMENT
#    as per descriptionn calculate.overlap "mostly complements the venn.diagram() 
#    function for the case where users want to know what values are grouped 
#    into the particular areas of the venn diagram."
#    The original version of this function does NOT return non-overlapping subsets for N=2. 
#    Instead it returns the entire lists
# REQUIRES
#    heatmaply, dendextend, seriation, assertthat
# VARIABLES
#    all the same as heatmapr
# RETURNS
#    a heatmapr object to be passed to heatmaply
calculate.overlap2 <- function (x) {
    if (1 == length(x)) {
        overlap <- x
    }
    else if (2 == length(x)) {
        
        ai <- intersect(x[[1]], x[[2]])
        
        overlap <- list(a1 = setdiff(x[[1]], ai), a2 = setdiff(x[[2]], ai), a3 = ai)
    }
    else if (3 == length(x)) {
        A <- x[[1]]
        B <- x[[2]]
        C <- x[[3]]
        nab <- intersect(A, B)
        nbc <- intersect(B, C)
        nac <- intersect(A, C)
        nabc <- intersect(nab, C)
        a5 = nabc
        a2 = nab[which(!nab %in% a5)]
        a4 = nac[which(!nac %in% a5)]
        a6 = nbc[which(!nbc %in% a5)]
        a1 = A[which(!A %in% c(a2, a4, a5))]
        a3 = B[which(!B %in% c(a2, a5, a6))]
        a7 = C[which(!C %in% c(a4, a5, a6))]
        overlap <- list(a5 = a5, a2 = a2, a4 = a4, a6 = a6, a1 = a1, 
                        a3 = a3, a7 = a7)
    }
    else if (4 == length(x)) {
        A <- x[[1]]
        B <- x[[2]]
        C <- x[[3]]
        D <- x[[4]]
        n12 <- intersect(A, B)
        n13 <- intersect(A, C)
        n14 <- intersect(A, D)
        n23 <- intersect(B, C)
        n24 <- intersect(B, D)
        n34 <- intersect(C, D)
        n123 <- intersect(n12, C)
        n124 <- intersect(n12, D)
        n134 <- intersect(n13, D)
        n234 <- intersect(n23, D)
        n1234 <- intersect(n123, D)
        a6 = n1234
        a12 = n123[which(!n123 %in% a6)]
        a11 = n124[which(!n124 %in% a6)]
        a5 = n134[which(!n134 %in% a6)]
        a7 = n234[which(!n234 %in% a6)]
        a15 = n12[which(!n12 %in% c(a6, a11, a12))]
        a4 = n13[which(!n13 %in% c(a6, a5, a12))]
        a10 = n14[which(!n14 %in% c(a6, a5, a11))]
        a13 = n23[which(!n23 %in% c(a6, a7, a12))]
        a8 = n24[which(!n24 %in% c(a6, a7, a11))]
        a2 = n34[which(!n34 %in% c(a6, a5, a7))]
        a9 = A[which(!A %in% c(a4, a5, a6, a10, a11, a12, a15))]
        a14 = B[which(!B %in% c(a6, a7, a8, a11, a12, a13, a15))]
        a1 = C[which(!C %in% c(a2, a4, a5, a6, a7, a12, a13))]
        a3 = D[which(!D %in% c(a2, a5, a6, a7, a8, a10, a11))]
        overlap <- list(a6 = a6, a12 = a12, a11 = a11, a5 = a5, 
                        a7 = a7, a15 = a15, a4 = a4, a10 = a10, a13 = a13, 
                        a8 = a8, a2 = a2, a9 = a9, a14 = a14, a1 = a1, a3 = a3)
    }
    else if (5 == length(x)) {
        A <- x[[1]]
        B <- x[[2]]
        C <- x[[3]]
        D <- x[[4]]
        E <- x[[5]]
        n12 <- intersect(A, B)
        n13 <- intersect(A, C)
        n14 <- intersect(A, D)
        n15 <- intersect(A, E)
        n23 <- intersect(B, C)
        n24 <- intersect(B, D)
        n25 <- intersect(B, E)
        n34 <- intersect(C, D)
        n35 <- intersect(C, E)
        n45 <- intersect(D, E)
        n123 <- intersect(n12, C)
        n124 <- intersect(n12, D)
        n125 <- intersect(n12, E)
        n134 <- intersect(n13, D)
        n135 <- intersect(n13, E)
        n145 <- intersect(n14, E)
        n234 <- intersect(n23, D)
        n235 <- intersect(n23, E)
        n245 <- intersect(n24, E)
        n345 <- intersect(n34, E)
        n1234 <- intersect(n123, D)
        n1235 <- intersect(n123, E)
        n1245 <- intersect(n124, E)
        n1345 <- intersect(n134, E)
        n2345 <- intersect(n234, E)
        n12345 <- intersect(n1234, E)
        a31 = n12345
        a30 = n1234[which(!n1234 %in% a31)]
        a29 = n1235[which(!n1235 %in% a31)]
        a28 = n1245[which(!n1245 %in% a31)]
        a27 = n1345[which(!n1345 %in% a31)]
        a26 = n2345[which(!n2345 %in% a31)]
        a25 = n245[which(!n245 %in% c(a26, a28, a31))]
        a24 = n234[which(!n234 %in% c(a26, a30, a31))]
        a23 = n134[which(!n134 %in% c(a27, a30, a31))]
        a22 = n123[which(!n123 %in% c(a29, a30, a31))]
        a21 = n235[which(!n235 %in% c(a26, a29, a31))]
        a20 = n125[which(!n125 %in% c(a28, a29, a31))]
        a19 = n124[which(!n124 %in% c(a28, a30, a31))]
        a18 = n145[which(!n145 %in% c(a27, a28, a31))]
        a17 = n135[which(!n135 %in% c(a27, a29, a31))]
        a16 = n345[which(!n345 %in% c(a26, a27, a31))]
        a15 = n45[which(!n45 %in% c(a18, a25, a16, a28, a27, 
                                    a26, a31))]
        a14 = n24[which(!n24 %in% c(a19, a24, a25, a30, a28, 
                                    a26, a31))]
        a13 = n34[which(!n34 %in% c(a16, a23, a24, a26, a27, 
                                    a30, a31))]
        a12 = n13[which(!n13 %in% c(a17, a22, a23, a27, a29, 
                                    a30, a31))]
        a11 = n23[which(!n23 %in% c(a21, a22, a24, a26, a29, 
                                    a30, a31))]
        a10 = n25[which(!n25 %in% c(a20, a21, a25, a26, a28, 
                                    a29, a31))]
        a9 = n12[which(!n12 %in% c(a19, a20, a22, a28, a29, a30, 
                                   a31))]
        a8 = n14[which(!n14 %in% c(a18, a19, a23, a27, a28, a30, 
                                   a31))]
        a7 = n15[which(!n15 %in% c(a17, a18, a20, a27, a28, a29, 
                                   a31))]
        a6 = n35[which(!n35 %in% c(a16, a17, a21, a26, a27, a29, 
                                   a31))]
        a5 = E[which(!E %in% c(a6, a7, a15, a16, a17, a18, a25, 
                               a26, a27, a28, a31, a20, a29, a21, a10))]
        a4 = D[which(!D %in% c(a13, a14, a15, a16, a23, a24, 
                               a25, a26, a27, a28, a31, a18, a19, a8, a30))]
        a3 = C[which(!C %in% c(a21, a11, a12, a13, a29, a22, 
                               a23, a24, a30, a31, a26, a27, a16, a6, a17))]
        a2 = B[which(!B %in% c(a9, a10, a19, a20, a21, a11, a28, 
                               a29, a31, a22, a30, a26, a25, a24, a14))]
        a1 = A[which(!A %in% c(a7, a8, a18, a17, a19, a9, a27, 
                               a28, a31, a20, a30, a29, a22, a23, a12))]
        overlap <- list(a31 = a31, a30 = a30, a29 = a29, a28 = a28, 
                        a27 = a27, a26 = a26, a25 = a25, a24 = a24, a23 = a23, 
                        a22 = a22, a21 = a21, a20 = a20, a19 = a19, a18 = a18, 
                        a17 = a17, a16 = a16, a15 = a15, a14 = a14, a13 = a13, 
                        a12 = a12, a11 = a11, a10 = a10, a9 = a9, a8 = a8, 
                        a7 = a7, a6 = a6, a5 = a5, a4 = a4, a3 = a3, a2 = a2, 
                        a1 = a1)
    }
    else {
        #flog.error("Invalid size of input object", name = "VennDiagramLogger")
        stop("Invalid size of input object")
    }
}

draw.pairwise.venn2 <- function (area1, area2, cross.area, category = rep("", 2), euler.d = TRUE, 
                                 scaled = TRUE, inverted = FALSE, ext.text = TRUE, ext.percent = rep(0.05, 3), 
                                 lwd = rep(2, 2), lty = rep("solid", 2), col = rep("black", 2), fill = NULL, 
                                 alpha = rep(0.5, 2), label.col = rep("black", 3), cex = rep(1, 3), 
                                 fontface = rep("plain", 3), fontfamily = rep("serif", 3), cat.pos = c(-50, 50), 
                                 cat.dist = rep(0.025, 2), cat.cex = rep(1, 2), cat.col = rep("black", 2), 
                                 cat.fontface = rep("plain", 2), cat.fontfamily = rep("serif", 2), 
                                 cat.just = rep(list(c(0.5, 0.5)), 2), cat.default.pos = "outer",  
                                 ext.pos = rep(0, 2), ext.dist = rep(0, 2), ext.line.lty = "solid", 
                                 ext.length = rep(0.95, 2), ext.line.lwd = 1, rotation.degree = 0, 
                                 rotation.centre = c(0.5, 0.5), ind = TRUE, sep.dist = 0.05, 
                                 offset = 0, cex.prop = NULL, print.mode = "raw", sigdigs = 3, 
                                 ...) 
{
    if (length(category) == 1) {
        category <- rep(category, 2)
    }
    else if (length(category) != 2) {
        stop("Unexpected parameter length for 'category'")
    }
    if (length(ext.percent) == 1) {
        ext.percent <- rep(ext.percent, 3)
    }
    else if (length(ext.percent) != 3) {
        stop("Unexpected parameter length for 'ext.percent'")
    }
    if (length(ext.pos) == 1) {
        ext.pos <- rep(ext.pos, 2)
    }
    else if (length(ext.pos) != 2) {
        stop("Unexpected parameter length for 'ext.pos'")
    }
    if (length(ext.dist) == 1) {
        ext.dist <- rep(ext.dist, 2)
    }
    else if (length(ext.dist) != 2) {
        stop("Unexpected parameter length for 'ext.dist'")
    }
    if (length(ext.length) == 1) {
        ext.length <- rep(ext.length, 2)
    }
    else if (length(ext.length) != 2) {
        stop("Unexpected parameter length for 'ext.length'")
    }
    if (length(lwd) == 1) {
        lwd <- rep(lwd, 2)
    }
    else if (length(lwd) != 2) {
        stop("Unexpected parameter length for 'lwd'")
    }
    if (length(lty) == 1) {
        lty <- rep(lty, 2)
    }
    else if (length(lty) != 2) {
        stop("Unexpected parameter length for 'lty'")
    }
    if (length(col) == 1) {
        col <- rep(col, 2)
    }
    else if (length(col) != 2) {
        stop("Unexpected parameter length for 'col'")
    }
    if (length(label.col) == 1) {
        label.col <- rep(label.col, 3)
    }
    else if (length(label.col) != 3) {
        stop("Unexpected parameter length for 'label.col'")
    }
    if (length(cex) == 1) {
        cex <- rep(cex, 3)
    }
    else if (length(cex) != 3) {
        stop("Unexpected parameter length for 'cex'")
    }
    if (length(fontface) == 1) {
        fontface <- rep(fontface, 3)
    }
    else if (length(fontface) != 3) {
        stop("Unexpected parameter length for 'fontface'")
    }
    if (length(fontfamily) == 1) {
        fontfamily <- rep(fontfamily, 3)
    }
    else if (length(fontfamily) != 3) {
        stop("Unexpected parameter length for 'fontfamily'")
    }
    if (length(fill) == 1) {
        fill <- rep(fill, 2)
    }
    else if (length(fill) != 2 & length(fill) != 0) {
        stop("Unexpected parameter length for 'fill'")
    }
    if (length(alpha) == 1) {
        alpha <- rep(alpha, 2)
    }
    else if (length(alpha) != 2 & length(alpha) != 0) {
        stop("Unexpected parameter length for 'alpha'")
    }
    if (length(ext.line.lwd) != 1) {
        stop("Unexpected parameter length for 'ext.line.lwd'")
    }
    if (length(cat.pos) == 1) {
        cat.pos <- rep(cat.pos, 2)
    }
    else if (length(cat.pos) != 2) {
        stop("Unexpected parameter length for 'cat.pos'")
    }
    if (length(cat.dist) == 1) {
        cat.dist <- rep(cat.dist, 2)
    }
    else if (length(cat.dist) != 2) {
        stop("Unexpected parameter length for 'cat.dist'")
    }
    if (length(cat.col) == 1) {
        cat.col <- rep(cat.col, 2)
    }
    else if (length(cat.col) != 2) {
        stop("Unexpected parameter length for 'cat.col'")
    }
    if (length(cat.cex) == 1) {
        cat.cex <- rep(cat.cex, 2)
    }
    else if (length(cat.cex) != 2) {
        stop("Unexpected parameter length for 'cat.cex'")
    }
    if (length(cat.fontface) == 1) {
        cat.fontface <- rep(cat.fontface, 2)
    }
    else if (length(cat.fontface) != 2) {
        stop("Unexpected parameter length for 'cat.fontface'")
    }
    if (length(cat.fontfamily) == 1) {
        cat.fontfamily <- rep(cat.fontfamily, 2)
    }
    else if (length(cat.fontfamily) != 2) {
        stop("Unexpected parameter length for 'cat.fontfamily'")
    }
    if (length(offset) != 1) {
        stop("Unexpected parameter length for 'offset'. 
             Try using 'rotation.degree' to achieve non-vertical offsets")
    }
    if (!(class(cat.just) == "list" & length(cat.just) == 2 & 
          length(cat.just[[1]]) == 2 & length(cat.just[[2]]) == 
          2)) {
        stop("Unexpected parameter format for 'cat.just'")
    }
    if (!euler.d & scaled) {
        stop("Uninterpretable parameter combination. 
             Please set both euler.d = FALSE and 
             scaled = FALSE to force Venn diagrams.")
    }
    if (offset > 1 | offset < 0) {
        stop("'Offset' must be between 0 and 1.  
             Try using 'rotation.degree = 180' 
             to achieve offsets in the opposite direction.")
    }
    if (cross.area > area1 | cross.area > area2) {
        stop("Impossible: cross section area too large.")
    }
    cat.pos <- cat.pos + rotation.degree
    if ((cat.default.pos != "outer") & (cat.default.pos != "text")) {
        cat.default.pos <- "outer"
    }
    
    max.circle.size = 0.2
    special.coincidental <- FALSE
    special.inclusion <- FALSE
    special.exclusion <- FALSE
    grob.list <- gList()
    tmp1 <- max(area1, area2)
    tmp2 <- min(area1, area2)
    area1 <- tmp1
    area2 <- tmp2
    r1 <- sqrt(area1/pi)
    r2 <- sqrt(area2/pi)
    if (r2 == 0) {
        r2 <- 0.5 * r1
    }
    shrink.factor <- max.circle.size/r1
    r1 <- r1 * shrink.factor
    r2 <- r2 * shrink.factor
    if (area1 == area2 & area2 == cross.area) {
        special.coincidental <- TRUE
    }
    if (cross.area != 0 & (cross.area == area2 | cross.area == 
                           area1)) {
        special.inclusion <- TRUE
    }
    if (0 == cross.area) {
        special.exclusion <- TRUE
    }
    denom <- area1 + area2 - cross.area
    wrapLab <- function(num) {
        stri = ""
        if (print.mode[1] == "percent") {
            stri <- paste(signif(num * 100/denom, digits = sigdigs), 
                          "%", sep = "")
            if (isTRUE(print.mode[2] == "raw")) {
                stri <- paste(stri, "\n(", num, ")", sep = "")
            }
        }
        if (print.mode[1] == "raw") {
            stri <- num
            if (isTRUE(print.mode[2] == "percent")) {
                stri <- paste(stri, "\n(", paste(signif(num * 
                                                            100/denom, digits = sigdigs), "%)", sep = ""), 
                              sep = "")
            }
        }
        return(stri)
    }
    if (scaled & !special.inclusion & !special.exclusion & !special.coincidental) {
        d <- find.dist(area1, area2, cross.area, inverted = inverted)
        d <- d * shrink.factor
        x.centre.1 <- (1 + r1 - r2 - d)/2
        x.centre.2 <- x.centre.1 + d
        tmp <- VennDiagram::ellipse(x = x.centre.1, y = 0.5, 
                                    a = ifelse(!inverted, r1, r2), b = ifelse(!inverted,  r1, r2), 
                                    gp = gpar(lty = 0, fill = fill[1], alpha = alpha[1]))
        grob.list <- gList(grob.list, tmp)
        tmp <- VennDiagram::ellipse(x = x.centre.2, y = 0.5, 
                                    a = ifelse(inverted, r1, r2), b = ifelse(inverted, r1, r2), 
                                    gp = gpar(lty = 0, fill = fill[2], alpha = alpha[2]))
        grob.list <- gList(grob.list, tmp)
        tmp <- VennDiagram::ellipse(x = x.centre.1, y = 0.5, 
                                    a = ifelse(!inverted, r1, r2), b = ifelse(!inverted, r1, r2), 
                                    gp = gpar(lwd = lwd[1], lty = lty[1], col = col[1], fill = "transparent"))
        grob.list <- gList(grob.list, tmp)
        tmp <- VennDiagram::ellipse(x = x.centre.2, y = 0.5, 
                                    a = ifelse(inverted, r1, r2), b = ifelse(inverted, r1, r2), 
                                    gp = gpar(lwd = lwd[2], lty = lty[2], col = col[2], fill = "transparent"))
        grob.list <- gList(grob.list, tmp)
        if (length(cex.prop) > 0) {
            if (length(cex.prop) != 1) {
                stop("Value passed to cex.prop is not length 1")
            }
            func = cex.prop
            if (class(cex.prop) != "function") {
                if (cex.prop == "lin") {
                    func = function(x) x
                }
                else if (cex.prop == "log10") {
                    func = log10
                }
                else {
                    stop(paste0("Unknown value passed to cex.prop: ", cex.prop))
                }
            }
            areas = c(area1 - cross.area, cross.area, area2 - 
                          cross.area)
            maxArea = max(areas)
            for (i in 1:length(areas)) {
                cex[i] = cex[i] * func(areas[i])/func(maxArea)
                if (cex[i] <= 0) 
                    stop(paste0("Error in rescaling of area labels: the label of area ", 
                                i, " is less than or equal to zero"))
            }
        }
        if (ext.text) {
            area.1.pos <- x.centre.1 + ifelse(!inverted, 
                                              -r1 + ((2 * r1 - (r1 + r2 - d))/2), 
                                              -r2 + ((2 * r2 - (r2 + r1 - d))/2))
            area.2.pos <- x.centre.2 + ifelse(!inverted, 
                                              r2 - ((2 * r2 - (r1 + r2 - d))/2), 
                                              r1 - ((2 * r1 - (r2 + r1 - d))/2))
            if ((area1 - cross.area)/area1 > ext.percent[1] & 
                (area1 - cross.area)/area2 > ext.percent[1]) {
                tmp <- textGrob(label = wrapLab(ifelse(!inverted, area1, area2) - cross.area), 
                                x = area.1.pos, y = 0.5, 
                                gp = gpar(col = label.col[1], cex = cex[1], 
                                          fontface = fontface[1], fontfamily = fontfamily[1]))
                grob.list <- gList(grob.list, tmp)
            }
            else {
                label.pos <- find.cat.pos(area.1.pos, 0.5, ext.pos[1], 
                                          ext.dist[1], r1)
                area.1.xpos <- label.pos$x
                area.1.ypos <- label.pos$y
                tmp <- textGrob(label = wrapLab(ifelse(!inverted, area1, area2) - cross.area),
                                x = area.1.xpos, y = area.1.ypos, 
                                gp = gpar(col = label.col[1], cex = cex[1], 
                                          fontface = fontface[1], fontfamily = fontfamily[1]))
                grob.list <- gList(grob.list, tmp)
                tmp <- linesGrob(x = c(area.1.pos + ext.length[1] * (area.1.xpos - area.1.pos), area.1.pos), 
                                 y = c(0.5 + ext.length[1] * (area.1.ypos - 0.5), 0.5), 
                                 gp = gpar(col = label.col[1], lwd = ext.line.lwd, 
                                           lty = ext.line.lty))
                grob.list <- gList(grob.list, tmp)
            }
            if ((area2 - cross.area)/area2 > ext.percent[2] & 
                (area2 - cross.area)/area1 > ext.percent[2]) {
                tmp <- textGrob(label = wrapLab(ifelse(inverted, area1, area2) - cross.area), 
                                x = area.2.pos, y = 0.5, 
                                gp = gpar(col = label.col[3], cex = cex[3], 
                                          fontface = fontface[3], fontfamily = fontfamily[3]))
                grob.list <- gList(grob.list, tmp)
            }
            else {
                label.pos <- find.cat.pos(area.2.pos, 0.5, ext.pos[2], ext.dist[2], r2)
                area.2.xpos <- label.pos$x
                area.2.ypos <- label.pos$y
                tmp <- textGrob(label = wrapLab(ifelse(inverted, area1, area2) - cross.area), 
                                x = area.2.xpos, y = area.2.ypos, 
                                gp = gpar(col = label.col[3], cex = cex[3], 
                                          fontface = fontface[3], fontfamily = fontfamily[3]))
                grob.list <- gList(grob.list, tmp)
                tmp <- linesGrob(x = c(area.2.pos + ext.length[1] * (area.2.xpos - area.2.pos), area.2.pos), 
                                 y = c(0.5 + ext.length[1] * (area.2.ypos - 0.5), 0.5), 
                                 gp = gpar(col = label.col[3], lwd = ext.line.lwd, lty = ext.line.lty))
                grob.list <- gList(grob.list, tmp)
            }
            if (cross.area/area2 > ext.percent[3] & cross.area/area1 > ext.percent[3]) {
                tmp <- textGrob(label = wrapLab(cross.area), 
                                x = x.centre.1 + (d - ifelse(!inverted, r2, r1)) + (r1 + r2 - d)/2, 
                                y = 0.5, gp = gpar(col = label.col[2], cex = cex[2], 
                                                   fontface = fontface[2], fontfamily = fontfamily[2]))
                grob.list <- gList(grob.list, tmp)
            }
            else {
                cross.area.pos <- x.centre.1 + (d - r2) + (r1 + r2 - d)/2
                cross.pos <- find.cat.pos(cross.area.pos, 0.5, ext.pos[1], ext.dist[1], r1 + r2)
                cross.area.xpos <- cross.pos$x
                cross.area.ypos <- cross.pos$y
                tmp <- textGrob(label = wrapLab(cross.area), 
                                x = cross.area.xpos, y = cross.area.ypos, 
                                gp = gpar(col = label.col[2], cex = cex[2], 
                                          fontface = fontface[2], fontfamily = fontfamily[2]))
                grob.list <- gList(grob.list, tmp)
                tmp <- linesGrob(x = c(cross.area.pos + ext.length[2] * (cross.area.xpos - cross.area.pos), 
                                       cross.area.pos), 
                                 y = c(0.5 + ext.length[2] * (cross.area.ypos - 0.5), 0.5), 
                                 gp = gpar(col = label.col[2], lwd = ext.line.lwd, lty = ext.line.lty))
                grob.list <- gList(grob.list, tmp)
            }
        }
        else {
            area.1.pos <- x.centre.1 + ifelse(!inverted, 
                                              -r1 + ((2 * r1 - (r1 + r2 - d))/2), 
                                              -r2 + ((2 * r2 - (r2 + r1 - d))/2))
            tmp <- textGrob(label = wrapLab(ifelse(!inverted, area1, area2) - cross.area), 
                            x = area.1.pos, y = 0.5, 
                            gp = gpar(col = label.col[1], cex = cex[1], 
                                      fontface = fontface[1], fontfamily = fontfamily[1]))
            grob.list <- gList(grob.list, tmp)
            area.2.pos <- x.centre.2 + ifelse(!inverted, 
                                              r2 - ((2 * r2 - (r1 + r2 - d))/2), 
                                              r1 - ((2 * r1 - (r2 + r1 - d))/2))
            tmp <- textGrob(label = wrapLab(ifelse(inverted, area1, area2) - cross.area), 
                            x = area.2.pos, y = 0.5, 
                            gp = gpar(col = label.col[3], cex = cex[3], 
                                      fontface = fontface[3], fontfamily = fontfamily[3]))
            grob.list <- gList(grob.list, tmp)
            tmp <- textGrob(label = wrapLab(cross.area), y = 0.5, 
                            x = x.centre.1 + (d - ifelse(!inverted, r2, r1)) + (r1 + r2 - d)/2, 
                            gp = gpar(col = label.col[2], cex = cex[2], 
                                      fontface = fontface[2], fontfamily = fontfamily[2]))
            grob.list <- gList(grob.list, tmp)
        }
        if ("outer" == cat.default.pos) {
            cat.pos.1 <- find.cat.pos(x.centre.1, 0.5, 
                                      (ifelse(!inverted, cat.pos[1], cat.pos[2]))%%360, 
                                      cat.dist[1], ifelse(!inverted, r1, r2))
            cat.pos.2 <- find.cat.pos(x.centre.2, 0.5, 
                                      (ifelse(!inverted, cat.pos[2], cat.pos[1]))%%360, 
                                      cat.dist[2], ifelse(!inverted, r2, r1))
        }
        else if ("text" == cat.default.pos) {
            cat.pos.1 <- find.cat.pos(area.1.pos, 0.5, cat.pos[1], cat.dist[1])
            cat.pos.2 <- find.cat.pos(area.2.pos, 0.5, cat.pos[2], cat.dist[2])
        }
        else {
            stop("Invalid value for 'cat.default.pos', should be either 'outer' or 'text'")
        }
        tmp <- textGrob(label = category[1], x = cat.pos.1$x, 
                        y = cat.pos.1$y, just = cat.just[[1]], 
                        gp = gpar(col = cat.col[1], cex = cat.cex[1], 
                                  fontface = cat.fontface[1], fontfamily = cat.fontfamily[1]))
        grob.list <- gList(grob.list, tmp)
        tmp <- textGrob(label = category[2], x = cat.pos.2$x, y = cat.pos.2$y, just = cat.just[[2]], 
                        gp = gpar(col = cat.col[2], cex = cat.cex[2], 
                                  fontface = cat.fontface[2], fontfamily = cat.fontfamily[2]))
        grob.list <- gList(grob.list, tmp)
    }
    if (euler.d & special.inclusion & !special.coincidental) {
        if (inverted) {
            tmp1 <- area1
            tmp2 <- area2
            area1 <- tmp2
            area2 <- tmp1
        }
        if (!scaled & !inverted) {
            r1 <- 0.4
            r2 <- 0.2
        }
        if (!scaled & inverted) {
            r1 <- 0.2
            r2 <- 0.4
        }
        tmp <- VennDiagram::ellipse(x = 0.5, y = 0.5, a = r1, b = r1, 
                                    gp = gpar(lty = 0, fill = fill[1], alpha = alpha[1]))
        grob.list <- gList(grob.list, tmp)
        tmp <- VennDiagram::ellipse(x = 0.5 - offset * (r1 - r2), y = 0.5, a = r2, b = r2, 
                                    gp = gpar(lty = 0, fill = fill[2], alpha = alpha[2]))
        grob.list <- gList(grob.list, tmp)
        tmp <- VennDiagram::ellipse(x = 0.5, y = 0.5, a = r1, b = r1, 
                                    gp = gpar(lwd = lwd[1], lty = lty[1], col = col[1], fill = "transparent"))
        grob.list <- gList(grob.list, tmp)
        tmp <- VennDiagram::ellipse(x = 0.5 - offset * (r1 - r2), y = 0.5, a = r2, b = r2, 
                                    gp = gpar(lwd = lwd[2], lty = lty[2], col = col[2], fill = "transparent"))
        grob.list <- gList(grob.list, tmp)
        area.2.pos <- 0.5 - offset * (r1 - r2)
        tmp <- textGrob(label = wrapLab(area2), x = area.2.pos, y = 0.5, 
                        gp = gpar(col = label.col[2], cex = cex[2],
                                  fontface = fontface[2], fontfamily = fontfamily[2]))
        grob.list <- gList(grob.list, tmp)
        if (!ext.text | !scaled) {
            area.1.pos <- (1 + r1 + r2 - offset * (r1 - r2))/2
            tmp <- textGrob(label = wrapLab(area1 - area2), x = area.1.pos, y = 0.5, 
                            gp = gpar(col = label.col[1], cex = cex[1],
                                      fontface = fontface[1], fontfamily = fontfamily[1]))
            grob.list <- gList(grob.list, tmp)
        }
        if (ext.text & scaled) {
            if (area2/area1 > 0.5) {
                area.1.pos <- (1 + r1 + r2 - offset * (r1 - r2))/2
                area.pos <- find.cat.pos(area.1.pos, 0.5, ext.pos[1], ext.dist[1], r1)
                area.1.xpos <- area.pos$x
                area.1.ypos <- area.pos$y
                tmp <- textGrob(label = wrapLab(area1 - area2), x = area.1.xpos, 
                                y = area.1.ypos, gp = gpar(col = label.col[1], cex = cex[1], 
                                                           fontface = fontface[1], fontfamily = fontfamily[1]))
                grob.list <- gList(grob.list, tmp)
                tmp <- linesGrob(x = c(area.1.pos + ext.length * (area.1.xpos - area.1.pos), 
                                       area.1.pos), y = c(0.5 + ext.length * (area.1.ypos - 0.5), 0.5), 
                                 gp = gpar(col = label.col[1], lwd = ext.line.lwd, lty = ext.line.lty))
                grob.list <- gList(grob.list, tmp)
            }
            else {
                area.1.pos <- (1 + r1 + r2 - offset * (r1 - r2))/2
                tmp <- textGrob(label = wrapLab(area1 - area2), x = area.1.pos, y = 0.5, 
                                gp = gpar(col = label.col[1], cex = cex[1], 
                                          fontface = fontface[1], fontfamily = fontfamily[1]))
                grob.list <- gList(grob.list, tmp)
            }
        }
        if (cat.default.pos == "outer") {
            cat.pos.1 <- find.cat.pos(0.5, 0.5, cat.pos[1], cat.dist[1], r1)
            cat.pos.2 <- find.cat.pos(0.5 - offset * (r1 - r2), 0.5, cat.pos[2], cat.dist[2], r2)
        }
        else if (cat.default.pos == "text") {
            cat.pos.1 <- find.cat.pos(area.1.pos, 0.5, cat.pos[1], cat.dist[1])
            cat.pos.2 <- find.cat.pos(area.2.pos, 0.5, cat.pos[2], cat.dist[2])
        }
        else {
            stop("Invalid value for 'cat.default.pos', should be either 'outer' or 'text'")
        }
        tmp <- textGrob(label = category[1], x = cat.pos.1$x, y = cat.pos.1$y, 
                        just = cat.just[[1]], gp = gpar(col = cat.col[1], cex = cat.cex[1], 
                                                        fontface = cat.fontface[1], 
                                                        fontfamily = cat.fontfamily[1]))
        grob.list <- gList(grob.list, tmp)
        tmp <- textGrob(label = category[2], x = cat.pos.2$x, y = cat.pos.2$y, 
                        just = cat.just[[2]], gp = gpar(col = cat.col[2], cex = cat.cex[2], 
                                                        fontface = cat.fontface[2], 
                                                        fontfamily = cat.fontfamily[2]))
        grob.list <- gList(grob.list, tmp)
    }
    if (euler.d & special.coincidental) {
        tmp <- VennDiagram::ellipse(x = 0.5, y = 0.5, a = max.circle.size, b = max.circle.size, 
                                    gp = gpar(lty = 0, fill = fill[1], alpha = alpha[1]))
        grob.list <- gList(grob.list, tmp)
        tmp <- VennDiagram::ellipse(x = 0.5, y = 0.5, a = max.circle.size, b = max.circle.size, 
                                    gp = gpar(lwd = lwd[1], lty = lty[1], col = col[1], fill = "transparent"))
        grob.list <- gList(grob.list, tmp)
        area.1.pos <- 0.46
        tmp <- textGrob(label = wrapLab(area1), x = area.1.pos,y = 0.5, 
                        gp = gpar(col = label.col[2], cex = cex[2], 
                                  fontface = fontface[2], fontfamily = fontfamily[2]))
        grob.list <- gList(grob.list, tmp)
        area.2.pos <- 0.54
        tmp <- textGrob(label = wrapLab(area2), x = area.2.pos, y = 0.5, 
                        gp = gpar(col = label.col[2], cex = cex[2], 
                                  fontface = fontface[2], fontfamily = fontfamily[2]))
        grob.list <- gList(grob.list, tmp)
        tmp <- textGrob(label = "(Coincidental)", x = 0.5, y = 0.45, 
                        gp = gpar(col = label.col[2], cex = cex[2], 
                                  fontface = fontface[2], fontfamily = fontfamily[2]))
        grob.list <- gList(grob.list, tmp)
        if (cat.default.pos == "outer") {
            cat.pos.1 <- find.cat.pos(0.5, 0.5, cat.pos[1], cat.dist[1], max.circle.size)
            cat.pos.2 <- find.cat.pos(0.5, 0.5, cat.pos[2], cat.dist[2], max.circle.size)
        }
        else if (cat.default.pos == "text") {
            cat.pos.1 <- find.cat.pos(area.1.pos, 0.5, cat.pos[1], cat.dist[1])
            cat.pos.2 <- find.cat.pos(area.2.pos, 0.5, cat.pos[2], cat.dist[2])
        }
        else {
            stop("Invalid value for 'cat.default.pos', should be either 'outer' or 'text'")
        }
        tmp <- textGrob(label = category[1], x = cat.pos.1$x, y = cat.pos.1$y, just = cat.just[[1]], 
                        gp = gpar(col = cat.col[1], cex = cat.cex[1], 
                                  fontface = cat.fontface[1], fontfamily = cat.fontfamily[1]))
        grob.list <- gList(grob.list, tmp)
        tmp <- textGrob(label = category[2], x = cat.pos.2$x, y = cat.pos.2$y, just = cat.just[[2]], 
                        gp = gpar(col = cat.col[2], cex = cat.cex[2], 
                                  fontface = cat.fontface[2], fontfamily = cat.fontfamily[2]))
        grob.list <- gList(grob.list, tmp)
    }
    if (euler.d & special.exclusion) {
        if (!scaled) {
            r1 <- 0.2
            r2 <- 0.2
        }
        x.centre.1 <- (1 - 2 * (r1 + r2))/2 + r1 - sep.dist/2
        tmp <- VennDiagram::ellipse(x = x.centre.1, y = 0.5, a = r1, b = r1, 
                                    gp = gpar(lty = 0, fill = fill[1], alpha = alpha[1]))
        grob.list <- gList(grob.list, tmp)
        x.centre.2 <- 1 - (1 - 2 * (r1 + r2))/2 - r2 + sep.dist/2
        tmp <- VennDiagram::ellipse(x = x.centre.2, y = 0.5, a = r2, b = r2, 
                                    gp = gpar(lty = 0, fill = fill[2], alpha = alpha[2]))
        grob.list <- gList(grob.list, tmp)
        tmp <- VennDiagram::ellipse(x = x.centre.1, y = 0.5, a = r1, b = r1, 
                                    gp = gpar(lwd = lwd[1], lty = lty[1], col = col[1], fill = "transparent"))
        grob.list <- gList(grob.list, tmp)
        tmp <- VennDiagram::ellipse(x = x.centre.2, y = 0.5, a = r2, b = r2, 
                                    gp = gpar(lwd = lwd[2], lty = lty[2], col = col[2], fill = "transparent"))
        grob.list <- gList(grob.list, tmp)
        area.1.pos <- x.centre.1
        tmp <- textGrob(label = wrapLab(area1), x = area.1.pos, y = 0.5, 
                        gp = gpar(col = label.col[1], cex = cex[1], 
                                  fontface = fontface[1], fontfamily = fontfamily[1]))
        grob.list <- gList(grob.list, tmp)
        area.2.pos <- x.centre.2
        tmp <- textGrob(label = wrapLab(area2), x = area.2.pos, 
                        y = 0.5, gp = gpar(col = label.col[3], cex = cex[3], 
                                           fontface = fontface[3], fontfamily = fontfamily[3]))
        grob.list <- gList(grob.list, tmp)
        if (cat.default.pos == "outer") {
            cat.pos.1 <- find.cat.pos(x.centre.1, 0.5, cat.pos[1], cat.dist[1], r1)
            cat.pos.2 <- find.cat.pos(x.centre.2, 0.5, cat.pos[2], cat.dist[2], r2)
        }
        else if (cat.default.pos == "text") {
            cat.pos.1 <- find.cat.pos(area.1.pos, 0.5, cat.pos[1], cat.dist[1])
            cat.pos.2 <- find.cat.pos(area.2.pos, 0.5, cat.pos[2], cat.dist[2])
        }
        else {
            stop("Invalid value for 'cat.default.pos', should be either 'outer' or 'text'")
        }
        tmp <- textGrob(label = category[1], x = cat.pos.1$x, 
                        y = cat.pos.1$y, just = cat.just[[1]], 
                        gp = gpar(col = cat.col[1], cex = cat.cex[1], 
                                  fontface = cat.fontface[1], fontfamily = cat.fontfamily[1]))
        grob.list <- gList(grob.list, tmp)
        tmp <- textGrob(label = category[2], x = cat.pos.2$x, 
                        y = cat.pos.2$y, just = cat.just[[2]], 
                        gp = gpar(col = cat.col[2], cex = cat.cex[2], 
                                  fontface = cat.fontface[2], fontfamily = cat.fontfamily[2]))
        grob.list <- gList(grob.list, tmp)
    }
    if ((!scaled & !euler.d) | (!scaled & euler.d & !special.inclusion & !special.exclusion & !special.coincidental)) {
        tmp <- VennDiagram::ellipse(x = 0.4, y = 0.5, a = max.circle.size, b = max.circle.size, 
                                    gp = gpar(lty = 0, fill = fill[1], alpha = alpha[1]))
        grob.list <- gList(grob.list, tmp)
        tmp <- VennDiagram::ellipse(x = 0.6, y = 0.5, a = max.circle.size, b = max.circle.size, 
                                    gp = gpar(lty = 0, fill = fill[2], alpha = alpha[2]))
        grob.list <- gList(grob.list, tmp)
        tmp <- VennDiagram::ellipse(x = 0.4, y = 0.5, a = max.circle.size, b = max.circle.size, 
                                    gp = gpar(lwd = lwd[1], lty = lty[1], col = col[1], fill = "transparent"))
        grob.list <- gList(grob.list, tmp)
        tmp <- VennDiagram::ellipse(x = 0.6, y = 0.5, a = max.circle.size, b = max.circle.size, 
                                    gp = gpar(lwd = lwd[2], lty = lty[2], col = col[2], fill = "transparent"))
        grob.list <- gList(grob.list, tmp)
        tmp <- textGrob(label = wrapLab(area1 - cross.area),  x = 0.3, y = 0.5, 
                        gp = gpar(col = label.col[1], cex = cex[1], 
                                  fontface = fontface[1], fontfamily = fontfamily[1]))
        grob.list <- gList(grob.list, tmp)
        tmp <- textGrob(label = wrapLab(area2 - cross.area), x = 0.7, y = 0.5, 
                        gp = gpar(col = label.col[3], cex = cex[3], 
                                  fontface = fontface[3], fontfamily = fontfamily[3]))
        grob.list <- gList(grob.list, tmp)
        tmp <- textGrob(label = wrapLab(cross.area), x = 0.5, y = 0.5, 
                        gp = gpar(col = label.col[2], cex = cex[2], 
                                  fontface = fontface[2], fontfamily = fontfamily[2]))
        grob.list <- gList(grob.list, tmp)
        if (cat.default.pos == "outer") {
            cat.pos.1 <- find.cat.pos(0.4, 0.5, cat.pos[1], cat.dist[1], max.circle.size)
            cat.pos.2 <- find.cat.pos(0.6, 0.5, cat.pos[2], cat.dist[2], max.circle.size)
        }
        else if (cat.default.pos == "text") {
            cat.pos.1 <- find.cat.pos(0.3, 0.5, cat.pos[1], cat.dist[1])
            cat.pos.2 <- find.cat.pos(0.7, 0.5, cat.pos[2], cat.dist[2])
        }
        else {
            stop("Invalid value for 'cat.default.pos', should be either 'outer' or 'text'")
        }
        tmp <- textGrob(label = category[1], x = cat.pos.1$x, 
                        y = cat.pos.1$y, just = cat.just[[1]], 
                        gp = gpar(col = cat.col[1], cex = cat.cex[1], 
                                  fontface = cat.fontface[1], fontfamily = cat.fontfamily[1]))
        grob.list <- gList(grob.list, tmp)
        tmp <- textGrob(label = category[2], x = cat.pos.2$x, 
                        y = cat.pos.2$y, just = cat.just[[2]], 
                        gp = gpar(col = cat.col[2], cex = cat.cex[2], 
                                  fontface = cat.fontface[2], 
                                  fontfamily = cat.fontfamily[2]))
        grob.list <- gList(grob.list, tmp)
    }
    grob.list <- adjust.venn(rotate.venn.degrees(grob.list, rotation.degree, 
                                                 rotation.centre[1], rotation.centre[2]), ...)
    if (ind) {
        grid.draw(grob.list)
    }
    return(grob.list)
}

#--------------
# FUNCTION
#    geneFromPathway
# COMMENT
#    A helper function that extract genes names for a specific Pathways and a contrast from QuSage output in KOvWT objec
# REQUIRES
#    stringi
#    
# VARIABLES
#    geneSet = the name of a gene set of interest eg,"REACTOME_NOTCH1_INTRACELLULAR_DOMAIN_REGULATES_TRANSCRIPTION"
#              will accept the vector of pathway names    
# RETURNS
#    a vector of gene names for a pathways (or a set of pathways) 

geneFromPathway <- function(geneSet){ 
    
    require("stringi")
    if(!is.vector(geneSet) | length(geneSet) == 0)stop("geneSet is either not a vector of empty")
    
    qsageIndex=grep("quSage",names(KOvWT))
    
    pathIndex <- sapply(geneSet,function(x) KOvWT[[qsageIndex[1]]]$PathwayIndexs[names(KOvWT[[qsageIndex[1]]]$PathwayIndexs) == x])
    
    pathGenes <- lapply(pathIndex, function(x)stringi::stri_trans_totitle(rownames(KOvWT[[qsageIndex[1]]]$GeneTable)[x]) ) %>% unlist %>% unique
    
    return(pathGenes)
}

#--------------
# FUNCTION
#    pathwayHeatmapFDR
# COMMENT
#    A helper function that performs K mean clustering of quSage FDR/Pvalues, creates a heatmap 
# using ComplexHeatmap package and  returns anobject that contains a ggplot object with a heatmap, 
# list of pathways for each clusters with associated pvalues/FDR and a least of genes for each identified pathway
# REQUIRES
#    "dplyr", "tibble", "reshape2", "ComplexHeatmap","circlize"
#    
# VARIABLES
#    kovwtObj - the name of KOvWT object. At the moment it is comented out and the use of KOvWT is hardwired)
#    km - the number of clusters for K-mean clustering (default =1)
#    clustering_distance_rows - distance method for gene clustering. The following distance 
#      (provided by ComplexHeatmap package) are available, "euclidean", "maximum", "manhattan",
#      "canberra", "binary", "minkowski", "pearson", "spearman", "kendall" 
#    clustering_method_rows - clustering methods for genes. THe following methods are available
#      "ward.D2", "single", "complete", "average", "mcquitty", "median","centroid"
#    fdrCutOff - "significance" cutoff for FDR/P value (default = 0.01)
#    minPathSize - the smallest number of genes in a pathway (default =10)
#    FDR - logical. IF TRUE FDR will be used as a "significance" metric otherwise the PValue will be used
# RETURNS
#    a list with four elements
#    `Heatmap of FDR values` - well, a heatmap
#    `Pathways in clusters` - if K mean clustering was used  a list of clusters with pathways
#    `Genes in selected pathways` - genes that belong to each pathways selected for the analysis and
#                                   shown on Heatmap
#    `Analysis constraints` - parameters of cluster analysis such as Clustering distance,
# Clustering method, metric (FDR or Pvalue),metric CutOff,Minimal number of genes in pathway,
# and the number of K mean clusters
# 
# EXAMPLE:
# Assuming KOvWT object if present in the memory
#
# test <- pathwayHeatmapFDR(km = 5, clustering_distance_rows = "minkowski", clustering_method_rows = "ward.D2", FDR=T)

pathwayHeatmapFDR <- function(
    #kovwtObj=NULL,
    km=1,
    clustering_distance_rows = c("euclidean", "maximum", "manhattan", "canberra", "binary", "minkowski", "pearson", "spearman", "kendall"),
    clustering_method_rows = c("ward.D2", "single", "complete", "average", "mcquitty", "median","centroid"),
    fdrCutOff = 0.01,
    minPathSize =10,
    FDR = T
){
    
    require ("dplyr", "tibble", "reshape2", "ComplexHeatmap", "circlize",character.only = T)
    
    if (length(clustering_distance_rows)!=1){warning("Clustering distance for pathways has not being selected. Set to \"minkowski\""); clustering_distance_rows = "minkowski"}
    if (length(clustering_method_rows)!=1){warning("Clustering method for pathways has not being selected. Set to \"ward.D2\""); clustering_method_rows = "ward.D2"}    
    
    
    qusageIndex <-  grep("quSage",names(KOvWT)) # indexes of all qsage entries in the object
    
    
    #print(qusageIndex)
    
    metric <- ifelse(FDR,"FDR","PValue")
    
    allPathwayFDR <-  sapply(qusageIndex,function(x)dplyr::select(KOvWT[[x]][[2]], metric), simplify=F,USE.NAMES = T) %>%
        as.data.frame %>% tibble::rownames_to_column(var = "rowname") %>%
        cbind.data.frame(.,size=KOvWT[[length(KOvWT)]][[2]]$size) %>%
        filter_at(vars(contains(metric)),any_vars(.<fdrCutOff)) %>%
        filter(.,size>minPathSize) %>%
        mutate_all(~replace(., is.na(.), 1)) %>%
        replace(., .== 0, min(.[,2:(ncol(.)-1)]) ) %>%
        filter(.,nchar(as.character(rowname))>10) %>% # removing strange short names
        select(.,-ncol(.)) %>%                        # remove size - it messes up heatmap
        mutate_at(.,vars(contains(metric)), ~(-log10(.))) %>%
        tibble::column_to_rownames(., var = "rowname") %>%
        set_colnames(.,colnames(KOvWT$contrastMatrix))
    
    hm2<- Heatmap(allPathwayFDR,
                  col = colorRamp2(c(min(allPathwayFDR),max(allPathwayFDR)), c("white" ,"darkred")),
                  #border = T,
                  show_row_names = F, 
                  row_title_rot = 0,
                  clustering_distance_rows = clustering_distance_rows,
                  clustering_method_rows = clustering_method_rows,
                  cluster_columns = T , 
                  #cluster_row=T,
                  gap=unit(3, "mm"),
                  name = "-Log10(FDR)",
                  #rect_gp = gpar(type="none"),
                  #km_title = "cl%i",
                  km=km)
    set.seed(123)
    hm2 <- draw(hm2) 
    
    clusters_allPathway <- lapply(ComplexHeatmap::row_order(hm2),function(x)allPathwayFDR[x,])
    
    selectedPath <- names(KOvWT[[qusageIndex[1]]][[3]]) %in% rownames(allPathwayFDR)
    
    genes_in_Pathway <- sapply(KOvWT[[qusageIndex[1]]][[3]][selectedPath],function(x)toupper(KOvWT$topTagTable$gene_name[x]),simplify = F, USE.NAMES = T)
    
    return(list(`Heatmap of FDR values` = hm2, 
                `Pathways in clusters` = clusters_allPathway, 
                `Genes in selected pathways` = genes_in_Pathway, 
                `Analysis constraints` = list( `Clustering distance` = clustering_distance_rows,
                                               `Clustering method` = clustering_method_rows,
                                               `Metric` = metric,
                                               `Metric CutOff` = fdrCutOff,
                                               `Minimal number of genes in pathway` = minPathSize,
                                               `Number of K mean clusters ` = km
                )))
    
    #heatmapPlot <- 
    #    heatmaply(x = allPathwayFDR, 
    #              colors =  c("white" ,"darkred"),
    #              margins = c(200, 400, 50, 0),
    #              plot_method = "plotly", 
    #              labCol = colnames(allPathwayFDR), 
    #              labRow = rownames(allPathwayFDR), 
    #              dist_method = clustering_distance_rows,
    #              hclust_method = clustering_method_rows,
    #              dendrogram = c("both"),
    #              fontsize_row = 6
    #key.title = switch(input$geneExprHeatScale, none = "logCPM", row = "Z-Score"),
    #scale = input$geneExprHeatScale,
    #col_side_colors = heatCovariates, 
    #             )
    
}

######################################################################################################

# FUNCTION
#    pathwayHeatmapFC
# COMMENT
#    A helper function that performs K mean clustering of quSage FoldChanges, creates a heatmap 
# using ComplexHeatmap package and  returns an object that contains a ggplot object with a heatmap, 
# list of pathways for each clusters with associated pvalues/FDR and a least of genes for each identified pathway
# This function is very similarl to pathwayHeatmapFDR
#
# REQUIRES
#    "dplyr", "tibble", "reshape2", "ComplexHeatmap","circlize"
#    
# VARIABLES
#    kovwtObj - the name of KOvWT object. At the moment it is comented out and the use of KOvWT is hardwired)
#    km - the number of clusters for K-mean clustering (default =1)
#    clustering_distance_rows - distance method for gene clustering. The following distance 
#      (provided by ComplexHeatmap package) are available, "euclidean", "maximum", "manhattan",
#      "canberra", "binary", "minkowski", "pearson", "spearman", "kendall" 
#    clustering_method_rows - clustering methods for genes. THe following methods are available
#      "ward.D2", "single", "complete", "average", "mcquitty", "median","centroid"
#    fdrCutOff - "significance" cutoff for FDR/P value (default = 0.01)
#    minPathSize - the smallest number of genes in a pathway (default =10)
#    FDR - logical. IF TRUE FDR will be used as a "significance" metric otherwise the PValue will be used
#    FCCutOff - logFC cutoff for the pathway. Set to 0 if you want ALL significant pathways
# RETURNS
#    a list with four elements
#    `Heatmap of FDR values` - well, a heatmap
#    `Pathways in clusters` - if K mean clustering was used  a list of clusters with pathways
#    `Genes in selected pathways` - genes that belong to each pathways selected for the analysis and
#                                   shown on Heatmap
#    `Analysis constraints` - parameters of cluster analysis such as Clustering distance,
# Clustering method, metric (FDR or Pvalue),metric CutOff,Minimal number of genes in pathway,
# and the number of K mean clusters
# 
# EXAMPLE:
# Assuming KOvWT object if present in the memory
#
# test <- pathwayHeatmapFC(kovwtObj = KOvWT,km = 6,clustering_distance_rows = "euclidean",clustering_method_rows = "ward.D2", FCCutOff = 0.3) 

pathwayHeatmapFC <- function(kovwtObj = NULL,
                             clustering_distance_rows = c("euclidean", "maximum", "manhattan", 
                                                          "canberra", "binary", "minkowski",
                                                          "pearson", "spearman", "kendall"),
                             clustering_method_rows = c("ward.D2", "single", "complete", 
                                                        "average", "mcquitty", "median",
                                                        "centroid"),
                             km = 1,
                             FCCutOff = 0.5,
                             fdrCutOff = 0.01,
                             FDR = T,
                             minPathSize = 10) {
    require("dplyr", quietly = T); require("tibble", quietly = T); require("reshape2", quietly = T);
    require("ComplexHeatmap", quietly = T); require("circlize", quietly = T);
    
    if (length(clustering_distance_rows)!=1){warning("Clustering distance for pathways has not being selected. Set to \"euclidean\""); clustering_distance_rows = "euclidean"}
    if (length(clustering_method_rows)!=1){warning("Clustering method for pathways has not being selected. Set to \"ward.D2\""); clustering_method_rows = "ward.D2"}    
    
    metric <- ifelse(FDR,"FDR","PValue")
    
    qusageIndex <-  grep("quSage",names(KOvWT)) # indexes of all qsage entries in the object
    
    allPathwayFC <-  sapply(qusageIndex,function(x)dplyr::select(KOvWT[[x]][[2]],c("logFC",metric)), simplify=F,USE.NAMES = T) %>% 
        as.data.frame %>% tibble::rownames_to_column(var = "rowname") %>%  
        cbind.data.frame(.,size=KOvWT[[length(KOvWT)]][[2]]$size) %>%
        filter_at(vars(contains(metric)),any_vars(.<fdrCutOff)) %>%    
        filter_at(vars(contains("logFC")),any_vars(abs(.)>FCCutOff)) %>% 
        filter(.,size>minPathSize) %>%
        select(.,-contains("FDR")) %>% 
        mutate_all(~replace(., is.na(.),0 ) ) %>%  
        filter(.,nchar(as.character(rowname))>10) %>% # removing strange short names
        select(.,-ncol(.)) %>%                        # remove size - it messes up heatmap 
        tibble::column_to_rownames(., var = "rowname") %>% 
        set_colnames(.,colnames(KOvWT$contrastMatrix))
    
    hm2<- Heatmap(allPathwayFC,
                  col = colorRamp2(c(min(allPathwayFC),0, max(allPathwayFC)), c("blue", "white" , "red")),
                  #border = T,
                  show_row_names = F, 
                  row_title_rot = 0,
                  clustering_distance_rows = clustering_distance_rows,
                  clustering_method_rows = clustering_method_rows,
                  cluster_columns = T , 
                  #cluster_row=T,
                  gap=unit(3, "mm"),
                  name = "Log2FC",
                  #rect_gp = gpar(type="none"),
                  #km_title = "cl%i",
                  km=km)
    set.seed(123)
    hm2 <- draw(hm2) 
    
    clusters_allPathway <- lapply(ComplexHeatmap::row_order(hm2),function(x)allPathwayFC[x,])
    
    selectedPath <- names(KOvWT[[qusageIndex[1]]][[3]]) %in% rownames(allPathwayFC)
    
    genes_in_Pathway <- sapply(KOvWT[[qusageIndex[1]]][[3]][selectedPath],function(x)toupper(KOvWT$topTagTable$gene_name[x]),simplify = F, USE.NAMES = T)
    
    #heatmapPlot <- 
    #    heatmaply(x = allPathwayFC, 
    #              colors =  c("blue", white" ,"dred"),
    #              margins = c(200, 400, 50, 0),
    #              plot_method = "plotly", 
    #              labCol = colnames(allPathwayFC), 
    #              labRow = rownames(allPathwayF), 
    #              dist_method = clustering_distance_rows,
    #              hclust_method = clustering_method_rows,
    #              dendrogram = c("both"),
    #              fontsize_row = 6
    #key.title = switch(input$geneExprHeatScale, none = "logCPM", row = "Z-Score"),
    #scale = input$geneExprHeatScale,
    #col_side_colors = heatCovariates, 
    #             )
    
    return(list(`Heatmap of FC values` = hm2, 
                `Pathways in clusters` = clusters_allPathway, 
                `Genes in selected pathways` = genes_in_Pathway, 
                `Analysis constraints` = list( `Clustering distance` = clustering_distance_rows,
                                               `Clustering method` = clustering_method_rows,
                                               `Metric` = metric,
                                               `Metric CutOff` = fdrCutOff,
                                               `Abs(Fold Change CutOff)` = FCCutOff,
                                               `Minimal number of genes in pathway` = minPathSize,
                                               `Number of K mean clusters ` = km
                )))
    
    
}


#--------------
# FUNCTION
#    plotlyText
# COMMENT
#    A convenience function for pretty text errors
# REQUIRES
#    plotly
# VARIABLES
#    displayText = a string that you would like displayed in the plotting area
# RETURNS
#    Nothing, used for it's side effect of plotting
plotlyText <- function(displayText) {
    plot_ly(data.frame(x = 0, y = 0), x = ~x, y = ~y,
            type = "scatter", 
            mode = "text", 
            text = displayText,
            textfont = list(color = '#000000', size = 16)) %>% 
        layout(xaxis = list(visible = F),
               yaxis = list(visible = F)) %>% 
        config(displayModeBar = FALSE)
}

plotText <- function(displayText){
    require(ggplot2, quietly = T)
    p <- ggplot() + 
        annotate("text", x = 4, y = 25, size = 8, label = displayText) + 
        theme_bw() +
        theme(axis.line = element_blank(),
              axis.text = element_blank(),
              axis.ticks = element_blank(),
              axis.title = element_blank(),
              panel.grid.major=element_blank(),
              panel.grid.minor=element_blank())
    print(p)
}

#--------------
# FUNCTION
#    plotlyText
# COMMENT
#    A convenience function for pretty text errors
# REQUIRES
#    plotly
# VARIABLES
#    displayText = a string that you would like displayed in the plotting area
# RETURNS
#    Nothing, used for it's side effect of plotting
# regNetForce <- function(genes = NULL, RegNetDB = NULL, species = c("Hs", "Mm", "any"), 
#                         nodeScaleParam = 2, colorTarg, colorTF, colorTFTarg, ...){
#     require(igraph, quietly = T); #require(networkD3, quietly = T); require(htmltools, quietly = T)
#     
#     if(is.null(RegNetDB)){
#         stop("Please supply a TF-Target netowrk with 'source', 'target', 'species'")
#     }
#     
#     Suace <- toupper(RegNetDB$source) %in% toupper(genes)
#     Targs <- toupper(RegNetDB$target) %in% toupper(genes)
#     specSpec <- switch(species,
#                        Hs = { RegNetDB$species == "h" },
#                        Mm = { RegNetDB$species == "m" },
#                        any = {RegNetDB$species != "any"})
#     
#     if(!any(Suace & Targs & specSpec)){
#         stop("No genes in your selection had TF <-> Target relationships")
#     }
#     
#     toPlot <- RegNetDB[Suace & Targs & specSpec, ]
#     toPlot$source <- toupper(toPlot$source)
#     toPlot$target <- toupper(toPlot$target)
# 
#     TFs <- unique(toPlot$source)
#     TargOTF <- unique(toPlot$target)
#     TFandTarg <- TFs[TFs %in% TargOTF]
#     
#     graphy <- 
#         as.vector(t(toPlot[,-3])) %>% 
#         make_graph(., directed = T)
#     
#     # this is a hack and slash means of colouring nodes based on linkage
#     # 1 -- TF and TF target, 2 -- TF only, 3 -- Target of TF only
#     clusty <- cluster_walktrap(graphy)
#     members <- membership(clusty) %>% names()
#     
#     myMember <- members 
#     myMember[myMember %in% TFandTarg] <- 1
#     myMember[myMember %in% TFs] <- 2
#     myMember[myMember %in% TargOTF] <- 3
#     myMember <- as.numeric(myMember)
#     names(myMember) <- members
#     
#     # currently only returns iGraph. Although interactive plot is probably a better option
#     return(list(IG = graphy, members = myMember))
#     
# }


#--------------
# FUNCTION
#    buildTFNet -- Given a topTagTable, build a network appropriate data structure for tfVizNet
# COMMENT
#    
# REQUIRES
#    RColorBrewer
# VARIABLES
#    x = a topTagTable presumably from out_list$topTagTable
#    tfDb = a transcription factor database with columns from, to, and species
# RETURNS
#    a list of length 3 containging $nodeInfo, $edgeInfo, and $flag
buildTFNet <- function(x, tfDb, addNodeInfo = NULL, species = "any", removeSelfLoops = TRUE, nnOnly = FALSE,
                       nodeColorColumn, colorBins){
    require(RColorBrewer, quietly = T)
    
    # make sure node and edge infor are all uppercase
    tfDb$from <- toupper(tfDb$from)
    tfDb$to <- toupper(tfDb$to)
    x$id <- toupper(x$id)
    flag = FALSE # initally set the flag to false
    
    if(removeSelfLoops){
      tfDb <- tfDb[tfDb$from != tfDb$to,]
    }
    
    if(is.null(x) || !"id" %in% colnames(x)){
        stop("Please supply a TF-Target node table with at least an 'id' column")
    }
    
    if(is.null(tfDb) || !all(c("from", "to", "species") %in% colnames(tfDb))){
        stop("Please supply a TF-Target edge table with at least 'from', 'to', and 'species' columns")
    }
    
    if(nnOnly && nrow(x) == 1){
        # for nearest neighbors search only in `from`
        testNetExist <- 
            toupper(tfDb$from) %in% toupper(x$id) &
            switch(species,
                   Hs = { tfDb$species == "h" },
                   Mm = { tfDb$species == "m" },
                   any = { tfDb$species != "any" })
        # for nearest neighbors search we need to get additional node information
        if(!is.null(addNodeInfo)){
            addNodeInfo$id <- toupper(addNodeInfo$gene_name)
            # colnames(addNodeInfo)[1] <- "id"
            # addNodeInfo$id <- toupper(addNodeInfo$id)
            addNodes <- tfDb[testNetExist, 1:2] %>% unlist(use.names = F) %>% unique()
            x <- addNodeInfo[addNodeInfo$id %in% addNodes,]
        }
    } else if(!nnOnly && nrow(x) != 1) {
        # for standard network require both to and from
        testNetExist <- 
            toupper(tfDb$from) %in% toupper(x$id) &
            toupper(tfDb$to) %in% toupper(x$id) &
            switch(species,
                   Hs = { tfDb$species == "h" },
                   Mm = { tfDb$species == "m" },
                   any = { tfDb$species != "any" })
    } else if(nnOnly && nrow(x) != 1){
        # if conflicting inputs, return an error generating node, edge set.
        nodes <- data.frame(id = 1:6)
        edges <- data.frame(from = rep(1, 5), to = c(2:6))
        flag = TRUE
    }
    
    if(any(testNetExist)){
        # if a valid network exists store only edge info related to that network
        edges <- tfDb[testNetExist,]
        if(species != "any"){
            edgeSpecies <- switch(species,
                                  Hs = { "h" },
                                  Mm = { "m" })
            edges <- edges[edges$species == edgeSpecies,]
        }
        # filter the node information to only connected nodes
        toGetNodes <- edges[,1:2] %>% unlist(use.names = F)
        x <- x[x$id %in% toGetNodes, ]
        # build nodes data.frame
        nodes <- x[, "id", drop = F ]
        nodes$label <- x[, "id"]
        nodes$TF <- nodes$id %in% edges$from
        nodes$Target <- nodes$id %in% edges$to
        nodes$TFT <- nodes$TF & nodes$Target
        nodes$group <- "Target"
        nodes$group[nodes$TF] <- "TF"
        nodes$group[nodes$TFT] <- "TFT"
        
        if(!is.null(nodeColorColumn)){
            nodes$color <- x[, nodeColorColumn]
            nodes[, nodeColorColumn] <- nodes$color
            if(!is.null(colorBins)){
                if(all(nodes$color < 1 & nodes$color > 0)){ # surrogate for guessing P-value or logFC
                    initPal <- rev(brewer.pal(11, "RdBu")[1:6])
                    nodes$color <- -log10(nodes$color)
                } else {
                    initPal <- rev(brewer.pal(11, "RdBu"))
                }
                finalPalFun <- colorRampPalette(initPal) 
                names(colorBins) <- finalPalFun(256)
                nodes$color <- names(colorBins)[findInterval(nodes$color, colorBins, all.inside = T)]
            }
        } 
        
    } else {
        nodes <- data.frame(id = 1:6)
        edges <- data.frame(from = rep(1, 5), to = c(2:6)) 
        flag = TRUE
    }
    
    return(list(nodeInfo = nodes, edgeInfo = edges, flag = flag))
}

#--------------
# FUNCTION
#    tfVizNet -- builds a visNetwork plot for buildTFNet output.
# COMMENT
#    
# REQUIRES
#    visNetwork
# VARIABLES
#    networkData = output from buildTFNet
# RETURNS
#    a visNetwork plot
tfVizNet <- function(networkData, edgeSmooth = FALSE, ...) { # 
    require(visNetwork, quietly = T); 
    
    if(networkData$flag){
        p <- visNetwork(networkData$nodeInfo, networkData$edgeInfo, 
                   main = list(text = "No TF <-> Target interactions\nidentified in your gene list.",
                               style = 'font-family:serif;font-weight:bold;font-size:36px;text-align:center;'), 
                   width = "100%")
        return(p)
    } else {
        
        lnodes = data.frame(label = c("TF+Target", "TF", "Target"),
                            shape = c("diamond", "triangle", "dot"),
                            color = c("#696969", "#696969", "#696969"),
                            size = c(10, 10, 10))
        p <- 
            visNetwork(networkData$nodeInfo, networkData$edgeInfo) %>% 
            visNodes(font = list(size = 22, color = "#000000")) %>% 
            visEdges(arrows = "to", smooth = TRUE,
                     color = list(color = "#696969", highlight = "#ff3333")) %>% 
            visPhysics(stabilization = FALSE) %>% 
            visInteraction(multiselect = TRUE) %>% 
            visExport(type = "pdf", background = "#ffffff") %>% 
            visLegend(width = 0.2, stepY = 75, useGroups = F, addNodes = lnodes) %>% 
            visGroups(p, groupname = "TFT", shape = "diamond") %>% 
            visGroups(p, groupname = "TF", shape = "triangle") %>% 
            visGroups(p, groupname = "Target", shape = "dot") # %>% visIgraphLayout()
        
        return(p)
    }
}

#--------------
# FUNCTION
#    allPairwiseIntersect
# COMMENT
#    calculate pathway intersections
# REQUIRES
#    idk yet
# VARIABLES
#    x = a qusage object from out_list
# RETURNS
#    a network? maybe...

allPairwiseIntersect <- function(x){
    require(magrittr); require(Matrix); require(igraph)
    allIntersects <- combn(1:length(x$PathwayIndexs), 2) %>% t()
    
    allJaccList <- lapply(1:nrow(allIntersects), function(i){
        jaccNum <- intersect(x$PathwayIndexs[[allIntersects[i,1]]], 
                             x$PathwayIndexs[[allIntersects[i,2]]]) %>% length()
        jaccDen <- union(x$PathwayIndexs[[allIntersects[i,1]]], 
                         x$PathwayIndexs[[allIntersects[i,2]]]) %>% length()
        jaccStat <- jaccNum/jaccDen
        return(jaccStat)
    })
    
    names(allJaccList) <- apply(allIntersects, 1, paste, collapse = "_")
    
    test <- unlist(allJaccList)
    
    S <- diag(length(x$PathwayIndexs))
    S[lower.tri(S, diag = F)] <- test
    diag(S) <- 0
    S[upper.tri(S)] <- t(S)[upper.tri(S)]
    isSymmetric(S)
    
    # G <- graph_from_adjacency_matrix(S, mode = "undirected", weighted = TRUE, diag = FALSE)
    # L <- layout_(G, layout = with_dh())
    # plot(G, layout = L, edge.width = E(G)$weight)
    
} 


