#!/usr/bin/R

################################################################################
# FUNCTION
#    getDE: Performs glm (no replicates)/QLF (default)/Treat (minLogFC != 0) 
#           model fitting and testing on a count matrix
# COMMENT
#    contrast should be generated with limma::makeContrasts() function 
#    alternatively it can be a vector with a single contrast to be tested.
# REQUIRES
#    edgeR, magrittr
# VARIABLES
#    counts = a count matrix
#    path.gmt = the path to the desired pathway DB in gmt format.
#    group = a factor defining the grouping of samples in counts
#    anno = the annotation for gene name conversion (should be generated from the GTF used during alignment) 
#    design = a design matrix (e.g. model.matrix( ~ 0 + group))
#    contrast = a vector or matrix of contrasts to test
#    nuisanceVars = a vector of nuisance variables for which normalized counts should be corrected
#                   if model.matrix(~ 0 + group + nuisance1 + batch1) then nuisanceVars = c("nuisance1", "batch1")
#    experiment = any meta-data about the experiment (for use with shiny server)
#    bioReps = an integer indicating the number of biological replicates (determines if glmFit is used)
#    cutoff = integer cutoff for minimal expression default is 3 cpm
#    minGroupCutoff = the minimum number of groups for which cutoff should be met
#    minLogFC = the minimum logFC to consider significant (determines if glmTreat is used)
#    log.t = should CPMs be log transformed
#    save.xlsx = should the result be saved in an excel workbook?
#    out.prefix = prefix for xlsx file if save.xlsx = T
#    doQuSage = should qusage be performed (default TRUE)
# RETURNS
#    list containing:
#        contrastMatrix = contrast -- the contrast matrix
#        rawCounts = counts -- a count matrix
#        transformedCounts = transformedCounts -- a library normalized CPM (can be log transformed) table
#        dgeList = y -- a DGEList object
#        dgeGLM = fit -- a DGEGLM object
#        dgeLRT = qlf -- a DGELRT object
#        topTags = tt -- a named list of topTags tables
#        topTagTable = a flattened table of the topTags list
################################################################################
getDE <- function(counts   = counts, 
                  path.gmt = gmt,
                  group    = group, 
                  anno     = anno,
                  design   = design, 
                  contrast = NULL, 
                  nuisanceVars    = NULL,
                  experiment      = NULL,
                  bioReps         = 3, 
                  cutoff          = 3,
                  minGroupCutoff  = 1,
                  minLogFC        = 0,
                  minPathwayGenes = 10,
                  maxPathwayGenes = 5000,
                  countNormMethod = c("cpm", "tpm"),
                  log.t         = TRUE,
                  prior         = 0.2,
                  out.prefix    = "summarizedDE",
                  save.xlsx     = TRUE,
                  doQuSage      = TRUE,
                  useFitMethod  = c("qlf", "lrt"),
                  useStatMethod = c("qlf", "lrt", "treat")) {
    
    require(plyr, quietly = T); require(magrittr, quietly = T); require(edgeR, quietly = T)
    require(writexl, quietly = T); require(qusage, quietly = T); require(parallel, quietly = T)
    require(sva, quietly = T)
    
    stopifnot(dim(contrast)[1] == dim(design)[2] & dim(design)[1] == dim(counts)[2])
    
    dispParam <- NULL
    
    if(any(contrast[1,] == -1) & all(design[,1] == 1)){
        stop("Contrast matrix specifies test against the design model's intercept.", call. = FALSE)
    }
    
    if( minLogFC != 0 ) { 
        warning("minLogFC != 0, setting useStatMethod to 'treat'\n",
                call. = FALSE, immediate. = TRUE)
        useStatMethod <- "treat" 
    }
    if( bioReps == 1 ) { 
        warning("bioReps == 1, setting useFitMethod to 'lrt'\n",
                call. = FALSE, immediate. = TRUE)
        useFitMethod <- 'lrt'
        bcv <- 0.4
        dispParam <- bcv^2
    }
    if( is.vector(contrast) ){ contrast <- matrix(contrast) }
    
    if(useFitMethod == "lrt" & useStatMethod == "qlf") {
        warning("useFitMethod 'lrt' inappropriate with useStatMethod 'qlf'. Defaulting to useFitMethod 'qlf'\n",
                call. = FALSE, immediate. = TRUE)
        useFitMethod <- "qlf"
    } else if (useFitMethod == "qlf" & useStatMethod == "treat") {
        warning("useFitMethod 'qlf' inappropriate with useStatMethod 'treat'. Defaulting to useFitMethod 'lrt'\n",
                call. = FALSE, immediate. = TRUE)
        useFitMethod <- "lrt"
    }
    
    #collect getDE argument for future storage
    passedArgs <- as.list(match.call())[-1] 
    passedArgs <- 
        rbind(names(passedArgs), as.character(unlist(passedArgs, use.names = F, recursive = T))) %>% 
        t() %>%
        set_colnames(c("Arg","Value"))
    
    tt <- list()
    y <- 
        DGEList(counts = counts, group = group, genes = anno) %>%
        .[rowSums(cpmByGroup(.) > cutoff) >= minGroupCutoff, , keep.lib.sizes = FALSE] %>%
        calcNormFactors(.) %>%
        estimateDisp(., design, robust = TRUE)
    
    fit <- switch(useFitMethod,
           qlf = {
               glmQLFit(y, design, robust = TRUE)
           },
           lrt = {
               glmFit(y, design, robust = TRUE, dispersion = dispParam)
           })
    
    for(i in 1:ncol(contrast)){
        tt[[i]] <- 
            switch(useStatMethod,
                   qlf = {
                       glmQLFTest(fit, contrast = contrast[, i])
                   },
                   lrt = {
                       glmLRT(fit, contrast = contrast[, i])
                   },
                   treat = {
                       glmTreat(fit, contrast = contrast[, i], lfc = minLogFC)
                   }) %>% 
            topTags(., sort.by = "none", n = Inf, adjust.method="BH") %>% .$table
    }
    names(tt) <- gsub(" - ", "-vs-", colnames(contrast)) # works for simple comparisons
    
    # START SUMMARIZE 
    filename <- paste(out.prefix, ".xlsx", sep = "")

    writeCounts <- data.frame(anno, counts)
    rawCounts <- data.frame(gene_name = anno$gene_name, counts, stringsAsFactors = F)
    stopifnot(identical(rownames(counts), writeCounts[,1]))
    
    writeFilteredCounts <- data.frame(y$genes, y$counts)
    filteredCounts <- data.frame(gene_name = y$genes$gene_name, y$counts, stringsAsFactors = F)
    
    if(countNormMethod == "cpm"){
        holdCPMs <- cpm(y, log = log.t, normalized.lib.sizes = TRUE, prior.count = prior)
        if(is.null(nuisanceVars)){
            writeTransformedCounts <- data.frame(y$genes, holdCPMs)
            transformedCounts <- data.frame(gene_name = y$genes$gene_name, holdCPMs, stringsAsFactors = F)
        } else {
            covars <- names(attributes(design)$contrasts)
            # the way ComBat works, we need an intercept model, so we regenerate the model
            # with an intercept if one without has been given
            # CONSIDER USING removeBatchEffect instead
            coreModel <- formula(paste("~", paste(covars[!covars %in% nuisanceVars], collapse = " + ")))
            correctedCPMs <- cpm(y, log = log.t, normalized.lib.sizes = TRUE, prior.count = prior) 
            for(i in 1:length(nuisanceVars)){
                currentBatch <- eval(parse(text = nuisanceVars[i]))
                batches <- list()
                for (j in 1:nlevels(currentBatch)) {
                    batches[[j]] <- which(currentBatch == levels(currentBatch)[j])
                }
                n.batches <- sapply(batches, length)
                correctedCPMs <- ComBat(correctedCPMs, batch = currentBatch, mod = model.matrix(coreModel))
            }
            
            writeTransformedCounts <- data.frame(y$genes, correctedCPMs)
            transformedCounts <- data.frame(gene_name = y$genes$gene_name, correctedCPMs, stringsAsFactors = F)
        }
    } else if(countNormMethod == "tpm"){
        warning("countNormMethod = 'tpm' does not support batch/nuisance variable adjustment")
        holdTPMs <- tpm(y$counts, anno = anno, Log = log.t, prior.count = prior)
        writeTransformedCounts <- data.frame(y$genes, holdTPMs)
        transformedCounts <- data.frame(gene_name = y$genes$gene_name, holdTPMs, stringsAsFactors = F)
    }
    # THIS NEEDS IMPROVMENT
    toptags <-
        do.call(cbind, 
                lapply(tt, "[", grep("^logFC|^PValue|^FDR", colnames(tt[[1]])))) %>%
        cbind(tt[[1]][, grep("^gene_name|^gene_type|^gene_biotype|^tag|^seqnames|^width|^logCPM", 
                             colnames(tt[[1]]))], .) %>% 
        as.data.frame()
    
    if(save.xlsx == T){
        writeTopTags <-
            do.call(cbind, 
                    lapply(tt, "[", grep("^logFC|^PValue|^FDR", colnames(tt[[1]])))) %>%
            cbind(tt[[1]][, grep("^gene_name|^gene_type|^gene_biotype|^tag|^seqnames|^width|^logCPM", 
                                 colnames(tt[[1]]))], .) %>% 
            as.data.frame %>% 
            cbind(gene_id = rownames(.), .)
        
        writeList <- list(All_Counts = writeCounts, 
                          Filtered_Counts = writeFilteredCounts,
                          Log2_CPMs = writeTransformedCounts, 
                          Differential_Expression_Results = writeTopTags)
    
        write_xlsx(x = writeList, path = filename, col_names = TRUE)
    }
    # fix some names to avoid doing it in app
    
    out <- list(experimentDetails = experiment,
                contrastMatrix = contrast,
                rawCounts = rawCounts, 
                filteredCounts = filteredCounts, 
                transformedCounts = transformedCounts,
                dgeList = y, 
                topTags = tt,
                topTagTable = toptags,
                analysisParam = passedArgs)
    
    # currently experimental...please report any issues to David Oliver
    # testing parallel version cause, fuck it takes time
    if( doQuSage == TRUE ){
        no_cores <- min(detectCores()/2, ncol(out$contrastMatrix))
        quClust <- makeCluster(no_cores)
        clusterEvalQ(cl = quClust, { source('DETools.r'); library(qusage); library(magrittr) })
        clusterExport(cl = quClust, varlist = c("path.gmt", "out", "minPathwayGenes", "maxPathwayGenes"), envir = environment())
        qusageObject <- parLapply(cl = quClust, 1:ncol(out$contrastMatrix),
                                  function(i) {
                                      qusage2(GMT = path.gmt, 
                                              DEG = out, contrast = i, 
                                              minGenes = minPathwayGenes, 
                                              maxGenes = maxPathwayGenes)
                                  })
        stopCluster(cl = quClust)
        names(qusageObject) <- gsub(" - ", "-vs-", colnames(out$contrastMatrix))
        
        out <- c(out, quSageResults = qusageObject)
    }
    colnames(out$contrastMatrix) <- gsub(" - ", "-vs-", colnames(out$contrastMatrix))
    # now returns a named list
    return(out)
}

#################################################################
# FUNCTION
#    Helper functions. Not necessary but sometimes useful 
# COMMENT
#    calculates the geometric mean with some an offset
# REQUIRES
#    
# VARIABLE
#    x = a vector of values
#    offset = a small offset to prevent NaN
# RETURNS
#    the geometric mean of x
#################################################################
gm <- function(x = counts, offset = 1){
    exp(mean(log(x + offset)))
}

# added is.integer0 as it is not used
is.integer0 <- function(x) {
    is.integer(x) && length(x) == 0L
}

# head and tail 
look <- function(x, n = 4){
    top <- head(x, n = n)
    er <- rep("...", ncol(x))
    bottom <- tail(x, n = n)
    print(top)
    cat(rep("", nchar(rownames(x)[1])), er, "\n")
    print(bottom)
}

# head for wide (big) data
bead <- function(x, n = 4){
    x[1:n,1:n]
}

# head of a list...the way you want head to work on large lists
lead <- function(x, n = 4){
    lapply(test, '[', 1:n)[1:n]
}

#################################################################
# FUNCTION
#    strToRegex
# COMMENT
#    I wrote this in one desperate moment 
# REQUIRES
#    magrittr
# VARIABLE
#    a character string
# RETURNS
#    a regex for the string passed? see comment above...
#################################################################
strToRegex <- function(x){
    require(magrittr, quietly = T)
    origStr <- x
    xChars <- strsplit(x, "") %>% unlist
    subCharIdx <- grep("[[:punct:]]", xChars)
    wsCharIdx <- grep("\\s", xChars)
    subChars <- xChars[subCharIdx]
    regexChars <- paste0("\\", subChars)
    xChars[subCharIdx] <- regexChars
    xChars[wsCharIdx] <- "\\s"
    regexStr <- paste(xChars, collapse = "")
    return(regexStr)
}

################################################################
# FUNCTION
#    tpm: calculates "transcipts" per million
# COMMENT
#    More precisely it normalizes genes by length
#    and then by library size
# REQUIRES
#    plyr, magrittr
# VARIABLE
#    x = the count matrix to normalize
#    anno = the annotation object (should correspond to the GTF for which features were counted)
#    log.t = log transform? (default = T)
#    prior = small prior to add before log transformation (default = 0.2)
# RETURNS
#    tmpMatrix = a matrix of TPM values
#################################################################
tpm <- function(x = counts, 
                anno = anno, 
                Log = TRUE, 
                prior.count = 0.2) {
    require(plyr, quietly = T)
    require(magrittr, quietly = T)
    if(substr(rownames(x)[1], start = 1, stop = 3) == "ENS"){
        x <- x[rownames(x) %in% anno$gene_id,]
        anno <- anno[match(rownames(x), anno$gene_id),]
        stopifnot(identical(rownames(x), anno$gene_id))
    } else {
        x <- x[rownames(x) %in% anno$gene_name,]
        anno <- anno[match(rownames(x), anno$gene_name),]
        stopifnot(identical(rownames(x), anno$gene_name))        
    }
    geneLengths <- 
        anno[, grep("gene_id|gene_name|width", colnames(anno))] %>% 
        plyr::mutate(., kbps = width/1000)
    lib.sizes <- colSums(x/geneLengths$kbps)/1e6
    x %<>%  
        divide_by(geneLengths$kbps) %>% 
        t() %>% divide_by(., c(lib.sizes)) %>% 
        t() %>% 
        `rownames<-` (., geneLengths$gene_id)

    if(Log == T){
        x <- log(x + prior.count)
    }
    return(x)
}

#################################################################
# FUNCTION
#    qusage2 is a wrapper for qusage and is built for use with
#    DETools function getDE()
# COMMENT
#    Theoretically this improves the output ¯\_(ツ)_/¯
# REQUIRES
#    qusage
#    magrittr
# VARIABLES
#    GMT = the path to a geneset file in gmt format
#    --> DEG = the output from the getDE() function <-- to be removed if this gets moved to getDE()
#    contrast = an integer specifying the contrast to be used (colnames(DEG$contrastMatrix)[contrast])
#    ... = additional parameters passed to qusage function
# RETURNS
#    qusageObject <- list(GeneTable = qusageGenes,
#                         PathwayTable = qusagePathways,
#                         PathwayIndexs = enrichedCanonPaths$pathways)
#################################################################

qusage2 <- function(GMT = "path/to/pathway.gmt", DEG, contrast = 1, minGenes = 5, maxGenes = 5000,...){
    require(qusage); require(magrittr)
    GMT <- read.gmt(file = GMT)
    # run qusage
    varEqual <- grepl("\\(|\\)", colnames(DEG$contrastMatrix)[contrast])
    #varEqual <- ifelse(any(grep("\\(|\\)", colnames(DEG$contrastMatrix))), TRUE, FALSE)
    
    validEset <-
        DEG$transformedCounts[,-1] %>%
        set_rownames(., toupper(DEG$transformedCounts$gene_name))
    
    tmpAggGS <- 
        makeComparison(eset = validEset,
                       var.equal = varEqual, 
                       labels = DEG$dgeList$samples$group,
                       contrast = colnames(DEG$contrastMatrix)[contrast]) %>% 
        aggregateGeneSet(geneResults = ., geneSets = GMT)
    
    # remove all pathways with 0 overlap in dataset. This fixes complex contrast issue
    GMT[which(tmpAggGS$path.size < minGenes)] <- NULL
    GMT[which(tmpAggGS$path.size > maxGenes)] <- NULL
    
    enrichedCanonPaths <- qusage(eset = validEset,
                                 var.equal = varEqual, 
                                 labels = DEG$dgeList$samples$group,
                                 contrast = colnames(DEG$contrastMatrix)[contrast],
                                 geneSets = GMT, ...)
    # get pathway results
    enrichedCanonPathsTop <- qsTable(enrichedCanonPaths, number = Inf, sort.by = "none")
    
    if(is.null(enrichedCanonPaths$mean)){
        qusageGenes <- data.frame(row.names = toupper(DEG$topTags[[contrast]]$gene_name),
                                  mean = DEG$topTags[[contrast]]$logFC, 
                                  sd = sqrt(DEG$dgeList$tagwise.dispersion), 
                                  alpha = 1)
    } else {
        qusageGenes <- data.frame(mean = enrichedCanonPaths$mean, 
                                  sd = enrichedCanonPaths$SD, 
                                  alpha = enrichedCanonPaths$sd.alpha)
    }

    qusagePathways <- data.frame(size = enrichedCanonPaths$path.size,
                                 logFC = enrichedCanonPaths$path.mean, 
                                 lowCI = calcBayesCI(enrichedCanonPaths)[1,],
                                 highCI = calcBayesCI(enrichedCanonPaths)[2,],
                                 PValue = enrichedCanonPathsTop$p.Value,
                                 FDR = enrichedCanonPathsTop$FDR)
    # filter pathways with small overlapping gene number
    toKeep <- rownames(qusagePathways)[qusagePathways$size >= minGenes]
    qusagePathways <- qusagePathways[rownames(qusagePathways) %in% toKeep,]
    pathsToKeep <- enrichedCanonPaths$pathways[names(enrichedCanonPaths$pathways) %in% toKeep]
    
    qusageObject <- list(GeneTable = qusageGenes,
                         PathwayTable = qusagePathways,
                         PathwayIndexs = pathsToKeep)

    return(qusageObject)
}

#################################################################
# FUNCTION
#   annoGen is a function for generating the annotation rds object
#   from a gencode GTF
# COMMENT
#    
# REQUIRES
#    genomation, edgeR, magrittr, GenomeInfoDb, plyr
# VARIABLE
#    GTF: the path to the GTF file to generate the RDS from
#    Chrs: the chromosomes to keep for the annotation e.g. paste0("Chr", c(1:22, "M", "X", "Y"))
#    columnNames: a vector of column names takes from the following, default columnNames=c("gene_id","gene_name","gene_type","tag","strand")
# "seqnames"                 "start"                    "end"                      "width"                    "strand"                   "source"                   "type"                    
# "score"                    "phase"                    "gene_id"                  "gene_type"                "gene_name"                "level"                    "havana_gene"             
# "transcript_id"            "transcript_type"          "transcript_name"          "transcript_support_level" "tag"                      "havana_transcript"        "exon_number"             
# "exon_id"                  "protein_id"               "ccdsid"                   "ont"        
# RETURNS
#    an annotation object which can be used for getDE()
#################################################################
gencodeAnnoGenerate <- function(GTF, Chrs, columnNames = c("gene_id", "gene_name",
                                                           "gene_type", "tag","seqnames",
                                                           "start", "end", "strand")){
    
    require(genomation); require(edgeR); require(magrittr); require(GenomeInfoDb); require(plyr)
    anno <-
        gffToGRanges(GTF) %>%
        keepSeqlevels(., Chrs, pruning.mode = "tidy") %>%
        as.data.frame() %>%
        .[grep("gene", .$type),columnNames] %>%
        plyr::mutate(., gene_id = strsplit2(gene_id, "\\.")[,1]) 
        return(anno)
}

#################################################################
# FUNCTION
#    buildExperiment generates the experimentalDetails portion
#    of the DRaMA output
# COMMENT
#    
# REQUIRES
#    data.table, magrittr
# VARIABLE
#    countMatrix: a count matrix
#    experimentName: a string containing the name of the experiment 
#    variableTable: a data.frame, tsv, csv, or txt table of variables. This table must have 
#                   rownames that are identical to the colnames of the countMatrix. 
#                   column names must be present
# RETURNS
#    an annotation object which can be used for getDE()
#################################################################
buildExperiment <- function(countMatrix, 
                            experimentName, 
                            variableTable,
                            excludeVarInNames = NULL){
    require(data.table); require(magrittr)
    
    if(is.character(variableTable)){
        variableTable <- 
            fread(input = variableTable, header = T, data.table = FALSE, stringsAsFactors = T) %>% 
            set_rownames(., .[,1]) %>% .[,-1]
    }
    
    if(!identical(rownames(variableTable), colnames(countMatrix))){
        stop("Rownames of variableTable are not identical to colnames of countMatrix")
    }
    
    if(!is.character(experimentName)){
        stop("Please enter a valid experimentName")
    }
    
    experimentLevels <- apply(variableTable, 2, function(x) length(unique(x)))
    
    keepLevels <- experimentLevels > 1
    if(!is.null(excludeVarInNames)){
        keepLevels[excludeVarInNames] <- FALSE
    }
    
    newNames <- apply(variableTable[,keepLevels], 1, paste, collapse = "_")
    
    originalNames <- colnames(countMatrix)
    rownames(variableTable) <- newNames

    experiment <- list()
    experiment$experimentID <- experimentName
    experiment$experimentVariables <- variableTable
    experiment$experimentLevels <- experimentLevels
    experiment$newNames <- newNames
    return(experiment)
    
}


#--------------
# FUNCTION
#    listPrereqs
# COMMENT
#    there are a lot of required packages. 
# REQUIRES
#    nothing
# VARIABLES
#    none
# RETURNS
#    a data.frame of required packages and their install status
listPrereqs <- function(){
    is.inst <- installed.packages()[,"Package"]
    req.paks <- c("plyr", "magrittr", "edgeR", "writexl", "qusage", "parallel",
                  "sva", "data.table", "GenomeInfoDb", "reshape2", 
                  "ggplot2", "scales", "dplyr", "heatmaply", "dendextend", "seriation",
                  "assertthat", "Matrix", "igraph", "visNetwork", "RColorBrewer", 
                  "tibble", "circlize", "ComplexHeatmap", "webshot", "htmlwidgets",
                  "VennDiagram", "stringi", "shiny", "shinydashboard", "shinycssloaders", 
                  "rlang", "DT", "crosstalk", "grid", "gridExtra", "UpSetR")
    cur.paks <- req.paks %in% is.inst
    res <- cbind.data.frame(required = req.paks, installed = cur.paks)
    return(res)
}


