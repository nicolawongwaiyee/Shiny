# Usage

## Repo Structure  
1. `backend` -- contains materials to get started  
   - `anno.rds` -- A rat annotation data.frame  
   - `c2.cp.v6.2.symbols.gmt` -- the c2 canonical pathways from MSigDb  
   - `countMatrix.tsv` -- a count matrix generated by STAR  
   - `DEAnalysis.r` -- script to generate the app object  
   - `DETestOutList.rds` -- the output from DEAnalysis.r  
   - `DETools.r` -- functions used in DEAnalysis.r  
   - `exampleVars.csv` -- an example variable file  
   - `RegNetwork_hu_mm_combined_no_mirna.csv` -- an interaction database for TF Networks  
   - `Shiny.Rproj` -- not useful, ignored  
2. `frontend` -- contains the app  
   - `app.R` -- the app  
   - `global.r` -- set of functions that the app uses  
   - `shiny.io.deployment.r` -- script for quickly getting an app onto shiny.io  
   - `www` -- a directory for CSS stuff  

## Getting Started 

1. Browsing the app with pre-canned data  
   - Copy the `DETestOutList.rds` and `RegNetwork_hu_mm_combined_no_mirna.csv` file 
     to the `frontend` directory
   - Start the app in R using `shiny::runApp("../frontend")`. Where `..` assumes 
     you started in `backend`, if not, simply substitue `..` for the full path to `frontend`.  
  
2. Running `DEAnalysis.r` with pre-canned data  
   - `DEanalysis.r` will run start to finish and generate two files starting with 
      `paste("edgeR_DEGs", Sys.Date(), sep = "_")`. 
     1. An `xlsx` file contains 4 tabs with the full analysis results. 
     2. An `rds` file containing the list object that the app uses. 
   - Copy the newly generated `rds` as well as the `RegNetwork_hu_mm_combined_no_mirna.csv` file
     to frontend. Run the app as before `shiny::runApp("../frontend")`.  

3. Running `DEAnalysis.r` with your data
   - What you will need.
     1. A count matrix -- we use STAR and the gencode GTF annotation
     2. An annotation data.frame -- This **MUST** contain at least 2 columns:
        the first being `gene_id` which matches the rownames of your count matrix,
        and the second being `gene_name` which contains human readable names. 
        * In an alternate universe we will fix this problem before it becomes a 
          dependency.
     3. A `.gmt` file from MsigDb where `gene_name` from the annotation file matches
        the gene names in the gmt file.
     4. (OPTIONAL): A network file with 3 columns `source`, `target`, and `species`
        `source` and `target` must match `gene_name` values. 
   - Assuming the above are met, things should be just peachy.
   
## Common Issues

1. QuSAGE is sensitive to all 0s (or other 0 variance situations) in groups that 
   it is comparing. This only occurs when using complex contrasts such as interactions. 
   If an error occurs increase `cutoff` and/or `minGroupCutoff`. Below is a copy
   of the error I got by setting `cutoff = 0`.
   - `2 nodes produced errors; first error: 'from' must be a finite number`
2. There are a LOT of libraries required and since packaging shiny apps is kind of
   a nightmare, I've included a function for checking all the pre-req packages.
   After sourcing `DETools.r`, run `listPrereqs()` just to be sure.

## Additional Documentation

https://chinenovy.gitlab.io/drama_manual/





